<?php

namespace App\Http\Controllers;

use App\Models\Kaldik;
use Illuminate\Http\Request;
use Throwable;

use function PHPUnit\Framework\throwException;

class KaldikController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        try {
            if (!$request->query('periode')) {
                $kaldiks = Kaldik::with('user')->get();
            } else {
                $periode = $request->query('periode');
                $semester = substr($periode,-1,1);
                $tapel = '20'. ($semester == 1 ? substr($periode, 0,2) : substr($periode,2,2));
                $query = Kaldik::query();
                if($semester == '1') {
                    $kaldiks = $query->where('is_active',1)->whereYear('start','=', $tapel)->whereMonth('start', '>', 6)->get();
                } else {
                    $kaldiks = $query->where('is_active',1)->whereYear('start','=', $tapel)->whereMonth('start', '<', 6)->get();
                }
                
            }
            return response()->json(['status' => 'success', 'kaldiks' => $kaldiks], 200);
        } catch (\Exception $e) {
            return response()->json(['status' => 'fail', 'msg' => $e->getMessage()], 500);
        }
    }


    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        try {
            $simpan = Kaldik::updateOrCreate(
                [
                    'id' => $request->id ?? null,

                ],
                [
                    'name' => $request->name,
                    'description' => $request->description,
                    'start' => $request->start,
                    'end' => $request->end,
                    'location' => $request->location ?? null,
                    'color' => $request->color ?? null,
                    'user_id' => $request->user()->name,
                    'is_libur' => $request->is_libur ?? null,
                    'is_active' => 1
                ]
            );
            return response()->json(['status' => 'success', 'kaldik' => $simpan], 200);
        } catch (\Exception $e) {
            return response()->json(['status' => 'error','message' => $e->getMessage()], 500);
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(Kaldik $kaldik)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Kaldik $kaldik)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Kaldik $kaldik)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Request $request, $id)
    {
        try {
            $kaldik = Kaldik::findOrFail($id);
            if($kaldik->user_id == $request->user()->name) {
                $kaldik->delete();
            } else {
                throw new \Exception("Ndak boleh ya..", 403);
            }
            return response()->json(['status' => 'success', 'msg' => 'Agenda dihapus'], 200);
        } catch(\Exception $e) {
            return response()->json(['status' => 'fail','msg' => $e->getMessage()],  $e->getCode());
        }
        
    }
}
