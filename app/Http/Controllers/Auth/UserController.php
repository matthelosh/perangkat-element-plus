<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Providers\RouteServiceProvider;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }


    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        try {
            $user = User::updateOrCreate(
                [
                    'id' => $request->id ?? null,
                    'name' => $request->name,
                    'email' => $request->email,
                ],
                [
                    'password' => Hash::make($request->password),
                    'level' => $request->level
                ]
            );
            return redirect('/login')->with('status', 'User berhasil dibuat');
        } catch(\Exception $e) {
            return redirect('/login')->withErrors('status', 'User gagal dibuat');
        }
    }

    public function login(Request $request)
    {
        try {
            $this->validate($request, [
                'name' => 'required',
                'password' => 'required'
            ]);

            $credentials = $request->only('name', 'password');

            if (Auth::attempt($credentials)) {
                $request->session()->regenerate();
                // return response()->json(['status' => 'success', 'user' => $request->user()]);
                return redirect()->intended(RouteServiceProvider::HOME);
            } else {
                throw new \Exception('Cek kembali Usernama atau password');
            }
        } catch (\Exception $e) {
            return back()->withErrors([
                'name' => 'Username atau Password tidak sesuai',
                'msg' => $e->getMessage()
            ]);
        }
    }

    public function logout(Request $request)
    {
        Auth::logout();
        $request->session()->invalidate();
        $request->regenerateToken();
        return redirect('/');
    }

    /**
     * Display the specified resource.
     */
    public function show(User $user)
    {
        try {
            //code...
        } catch (\Throwable $th) {
            //throw $th;
        }
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(User $user)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, User $user)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(User $user)
    {
        //
    }
}
