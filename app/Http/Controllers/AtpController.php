<?php

namespace App\Http\Controllers;

use App\Models\Atp;
use App\Models\ModulAjar;
use Illuminate\Support\Str;
use Illuminate\Http\Request;

class AtpController extends Controller
{
    //
    public function index(Request $request) {
        
    }

    public function store(Request $request) {
        try {
            $data = json_decode($request->data);
            // dd($data);
            $urut = 0;
            foreach($data->atps as $atp) 
            {
                $urut+=1;
                $store = Atp::updateOrCreate(
                    [
                        'id'        => $atp->id         ?? null,
                        'kode'      => $atp->kode == null ? $data->cp_id."-".$urut : $atp->kode,
                        'cp_id'     => $data->cp_id      ,
                        'rombel_id' => $data->rombel_id  ?? null,
                        'guru_id'   => $data->guru_id    ?? null,
                        'urut'      => $urut    ,
                    ],
                    [
                        'tps'       => $atp->tps,
                        'materi'    => $atp->materi  ,
                        'tingkat'   => $atp->tingkat ,
                        'aw'        => $atp->aw      ,
                        'semester'  => $atp->semester,
                        'asesmen'   => $atp->asesmen
                    ]
                );
                // dd($store);
                if ($store) {
                    ModulAjar::create([
                        'kode' => 'MA-'.$store->kode,
                        'atp_id' => $store->kode,
                        'tingkat' => $atp->tingkat
                    ]);
                }
            }
            return response()->json(['status' => 'Ok', 'message' => 'ATP Disimpan'], 200);
        } catch (\Throwable $th) {
            //throw $th;
            dd($th);
        }
    }
}
