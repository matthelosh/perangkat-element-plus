<?php

namespace App\Http\Controllers;

use App\Models\Prosem;
use Illuminate\Http\Request;

class ProsemController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        
    }


    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $prosems = $request['data']['prosems'];
        try {
            foreach($prosems as $k=>$v) {
                $kode = explode("-",$k);
                $pembelajaran_id = $kode[0]."-".$kode[1]."-".$kode[2]."-".$kode[3];
            Prosem::updateOrCreate(
                    [
                        'id' => $v['id'] ?? null,
                        'materi_id' => $k
                    ],
                    [
                        'guru_id' => $request['data']['guru_id'],
                        'pembelajaran_id' => $pembelajaran_id,
                        'rombel_id' => $request['data']['rombel_id'],
                        'tanggal' => $v['tanggal'],
                        'minggu_ke' => $v['minggu_ke'],
                        'hari' => $v['hari']
                    ]
                );
            }

            return response()->json(['status' => 'success', 'msg' => 'Prosem Disimpan'], 200);
        } catch (\Exception $e) {
            return response()->json(['status' => 'failed', 'msg' => $e->getMessage()], 500);
        }
        
    }

    /**
     * Display the specified resource.
     */
    public function show(Prosem $prosem)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Prosem $prosem)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Prosem $prosem)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Prosem $prosem)
    {
        //
    }
}
