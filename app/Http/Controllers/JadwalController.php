<?php

namespace App\Http\Controllers;

use App\Models\Jadwal;
use Illuminate\Http\Request;

class JadwalController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        try {
            switch(auth()->user()->level) {
                default:
                    $jadwals = Jadwal::all();
                    break;
                case "gpai":
                    $jadwals = Jadwal::where('guru_id', auth()->user()->guru->id)->with('rombel')->orderBy('jamke', 'ASC')->get();
                    break;
            }
            return response()->json(['status' => 'success', 'jadwals' => $jadwals],200);
        } catch(\Exception $e) {
            return response()->json(['status' => 'fail', 'msg' => $e->getMessage()],500);
        }
    }


    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        // dd($request->all());
        try {
            $jadwal = Jadwal::updateOrCreate(
                [
                    'id' => $request->jadwal['id'] ?? null
                ],
                [
                    'sekolah_id' => auth()->user()->guru->sekolah_id,
                    'rombel_id' => $request->jadwal['rombel_id'],
                    'guru_id' => auth()->user()->guru['id'],
                    'jamke' => $request->jadwal['jamke'],
                    'hari' => $request->jadwal['hari']
                ]
            );
            return response()->json(['status' => 'success', 'jadwal' => $jadwal],200);
        } catch(\Exception $e) {
            return response()->json(['status' => 'fail', 'msg' => $e->getMessage()],500);
        }
    }

    public function update(Request $request, $id)
    {
        try {
            $jadwal = Jadwal::findOrFail($id)->update(['rombel_id' => $request->jadwal['rombel_id']]);
            return response()->json(['status' => 'success', 'delete' => $jadwal], 200);
        } catch (\Exception $e) {
            return response()->json(['status' => 'fail', 'msg' => $e->getMessage()], 500);
        }
    }

    /**

     * Remove the specified resource from storage.
     */
    public function destroy(Jadwal $jadwal, $id)
    {
        try {
            $jadwal->findOrFail($id)->delete();
            return response()->json(['status' => 'success', 'delete' => $jadwal], 200);
        } catch (\Exception $e) {
            return response()->json(['status' => 'fail', 'msg' => $e->getMessage()], 500);
        }
    }
}
