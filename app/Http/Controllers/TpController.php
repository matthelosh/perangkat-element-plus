<?php

namespace App\Http\Controllers;

use App\Models\Cp;
use App\Models\Rombel;
use App\Models\Tp;
use Inertia\Inertia;
use Illuminate\Http\Request;

class TpController extends Controller
{

    public function home(Request $request) {
        $rombel = Rombel::where('kode_rombel', $request->query('rombel_id'))->first();
        return Inertia::render('Dashboard/Rencana/Tp', [
            'rombel' => $rombel,
            'cps' => Cp::where('fase', $rombel->fase)->with('tps', function($q) use($request) {
                $q->where('guru_id', ($request->query('mine') == 'true' ? $request->user()->guru->nip : null));
            })->with('atps.ma')->get()
        ]);
    }

    public function store(Request $request) {
        try {
            $cp = $request->cp;
            $i =  0;
            $tps = [];
            // dd($request->query('mine'));
            foreach( $cp['tps'] as $tp ) 
            {
                
                ($kode = ($request->query('mine')=='1') ? $request->user()->guru->nip.'-'.substr($cp['kode'],0,2).'-'.$i+1 : substr($cp['kode'],0,2).'-'.$i+1 );
                
                Tp::updateOrCreate(
                    [
                        'id' => $tp['id'] ?? null,
                        'kode' => $kode,
                        'cp_id' => $cp['kode'],
                        'fase' => $cp['fase'],
                        'guru_id' => $request->query('mine') == '1' ? $request->user()->guru->nip : null,
                    ],
                    [
                        'kompetensi' => $tp['kompetensi'],
                        'materi' => $tp['materi'],
                        'teks' => $tp['teks'],
                    ]
                    );

                    $i++;
                
            }

            return response()->json(['status' => 'success','msg' => 'Tujuan Pembelajaran disimpan'], 200);
        } catch (\Throwable $th) {
            return response()->json(['status' => 'fail','msg' => $th->getMessage()],500);
        }
    }

    public function destroy(Request $request) {
        try {
            $tp = Tp::findOrFail($request->id);
            $tp->delete();
            return response()->json(['status' => 'success','msg' => 'Tujuan Pembelajaran dihapus'], 200);
        } catch (\Throwable $th) {
            return response()->json(['status' => 'fail','msg' => $th->getMessage()],500);
        }
    }
}
