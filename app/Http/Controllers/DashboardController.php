<?php

namespace App\Http\Controllers;

use App\Models\Guru;
use Inertia\Inertia;
use App\Models\Rombel;
use App\Models\Periode;
use App\Models\Sekolah;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function home(Request $request){

        return Inertia::render('Dashboard/Home', [
            'rombels' => $this->rombels_without_jadwals($request->user()),
            'gurus' => $request->user()->level == 'admin' ? Guru::whereNot('id', 1)->get() : null,
            'dataSekolah' => Sekolah::with('rombels.siswas', 'siswas')->get()
        ]);
    }

    public function rencana(Request $request) {
        return Inertia::render('Dashboard/Rencana/Index', [
            'rombels' => $this->rombels_without_jadwals($request->user())
        ]);
    }
    
    public function pelaksanaan(Request $request) {
        return Inertia::render('Dashboard/Pelaksanaan/Index', [
            'rombels' => $this->rombels($request->user())
        ]);
    }

    public function guru(Request $request) {
        return Inertia::render('Dashboard/Utama/Guru', [
            'gurus' => Guru::whereNot('id', '1')->with('sekolah', 'user')->get()
        ]);
    }

    function periode() {
        return Periode::where('is_active', 1)->first();
    }

    function rombels($user) {
        return Rombel::where([
            ['guru_id','=',$user->guru->id],
            ['periode_id','=',$this->periode()->kode_periode]
           ])->has('jadwals')->with('jadwals','siswas')->get();
    }

    function rombels_without_jadwals($user) {
        return Rombel::where([
            ['guru_id','=',$user->guru->id],
            ['periode_id','=',$this->periode()->kode_periode]
           ])->with('jadwals','siswas')->get();
    }
    
}


