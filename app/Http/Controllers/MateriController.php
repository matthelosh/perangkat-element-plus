<?php

namespace App\Http\Controllers;

use App\Models\Prosem;
use App\Models\Prota;
use DateTime;
use Illuminate\Http\Request;

class MateriController extends Controller
{
    public function storeProtasem(Request $request) {
        $dates =[];
        try {
            foreach($request->protas as $k => $v ) {
                $kode = explode("-", $k);
                $pembelajaran_id = $kode[0]."-".$kode[1]."-".$kode[2]."-".$kode[3];
                Prota::updateOrCreate(
                    [
                        'materi_id' => $k,
                        'pembelajaran_id' => $pembelajaran_id,
                        'rombel_id' => $request->rombel
                    ],[
                        'aw' => $v
                    ]
                );
            }
            foreach($request->prosems as $k => $v ) {
                $kode = explode("-", $k);
                $pembelajaran_id = $kode[0]."-".$kode[1]."-".$kode[2]."-".$kode[3];
                $date = new DateTime($v);
                array_push($dates, $date);
                Prosem::updateOrCreate(
                    [
                        'materi_id' => $k,
                        'pembelajaran_id' => $pembelajaran_id,
                        'rombel_id' => $request->rombel,
                        'guru_id' => $request->guru_id,
                        'hari' => $request->hari,
                    ],[
                        
                        'tanggal' => $v,
                        'minggu_ke' => (int) $date->format("W"),
                    ]
                );
            }

            return response()->json(['status' => 'Ok', 'msg' => $dates], 200);
        } catch(\Exception $e) {
            dd($e);
        }
    }
}
