<?php

namespace App\Http\Controllers;

use App\Models\Cp;
use Illuminate\Http\Request;

class CpController extends Controller
{
    public function index(Request $request) {
        try {
            return response()->json([
                'status' => 'success',
                'cps' => Cp::where('fase', $request->fase)->with('tps', function($q) use($request) {
                    $q->where('guru_id', ($request->query('mine') ? $request->user()->guru->nip : null));
                })->with('atps.ma')->get()
            ], 200);
        } catch (\Throwable $th) {
            dd($th);
        }
    }
}
