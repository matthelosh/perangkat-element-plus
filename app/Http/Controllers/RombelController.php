<?php

namespace App\Http\Controllers;

use Inertia\Inertia;
use App\Models\Siswa;
use App\Models\Rombel;
use App\Models\Periode;
use Illuminate\Http\Request;
use App\Http\Requests\SiswaRequest;

class RombelController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        try {
            switch($request->user()->level) {
                default:
                    $rombels = Rombel::orderBy('sekolah_id', 'ASC')->with('siswas', 'sekolah')->get();
                    break;
                case "gpai":
                    $rombels = Rombel::where('guru_id', $request->user()->guru->id)->orderBy('tingkat', 'ASC')->with('siswas')->get();
                    break;
            }

            return response()->json(['status' => 'success', 'rombels' => $rombels], 200);
        } catch (\Exception $e) {
            return response()->json(['status' => 'fail', 'msg' => $e->getMessage()], 500);
        }
    }

    public function page(Request $request) {
        return Inertia::render('Dashboard/Utama/Rombel', [
            'myRombels' => Rombel::orderBy('tingkat')
                                ->where('periode_id', $this->periode()->kode_periode)
                                ->where('guru_id',$request->user()->guru->id)
                                ->with('jadwals', 'siswas')
                                ->get(),
        ]);
    }


    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        try {
            $data = $request->all();
            if(isset($data['id'])) {
                Rombel::find($data['id'])->siswas()->update(['rombel_id' => $data['kode_rombel']]);
            }
            $rombel = Rombel::updateOrCreate(
                [
                    'id' => $data['id'] ?? null
                ],
                [
                    'kode_rombel' => $data['kode_rombel'] ?? null,
                    'label' => $data['label'] ?? null,
                    'tingkat' => $data['tingkat'] ?? null,
                    'sekolah_id' => $data['sekolah_id'] ?? null,
                    'guru_id' => $data['guru_id'] ?? null,
                    'periode_id' => $data['periode_id'] ?? null,
                    'fase' => $data['fase'] ?? null
                ]
            );
            // dd($rombel);
            // $rombel->siswas()->update(['rombel_id' => $rombel->kode_rombel]);
            return back()->with('status', 'success');
        } catch(\Exception $e) {
            return back()->withErrors(['status' => 'fail', 'msg' => $e->getMessage(), 'errCode' => $e->getCode()], 500);
        }
    }

    public function imporSiswa(Request $request)
    {
        try {
            $siswas = $request->data;
            foreach($siswas as $siswa) {
                Siswa::updateOrCreate(
                    [
                        'nisn'=> $siswa['nisn'] ?? null,
                    ],
                    [
                        'nama'=> $siswa['nama'] ?? null,
                        'jk'=> $siswa['jk'] ?? null,
                        'sekolah_id'=> $request->sekolah_id ?? null,
                        'rombel_id'=> $request->rombel_id ?? null,
                        
                    ]
                );

            }
            return response()->json(['status' => 'success', 'msg' => 'Siswa diimpor'], 200);
        } catch (\Exception $e) {
            return response()->json(['status' => 'fail', 'msg' => $e->getMessage()], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Rombel $rombel, $id)
    {
        try {
            $rombel = Rombel::findOrFail($id);
            $rombel->siswas()->delete();
            $rombel->delete();
            return response()->json(['status' => 'success', 'msg' => 'Rombel dihapus'], 200);
        } catch (\Throwable $th) {
            return response()->json(['status' => 'fail', 'msg' => $th->getMessage()], 500);
        }
    }

    function periode() {
        return Periode::where('is_active', '1')->first();
    }
}
