<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tp extends Model
{
    use HasFactory;

    protected $fillable = [
        'kode',
        'cp_id',
        'guru_id',
        'fase',
        'kompetensi',
        'materi',
        'teks'
    ];

    function cp() {
        return $this->belongsTo(Cp::class, 'cp_id', 'kode');
    }
}
