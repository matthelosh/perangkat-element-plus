<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ModulAjar extends Model
{
    use HasFactory;

    protected $fillable = [
        'kode',
        'tingkat',
        'atp_id',
        'rombel_id',
        'guru_id',
        'tanggal',
        'fase',
        'materi',
        'kompetensi_awal',
        'p5',
        'sarpras',
        'target_siswa',
        'model_pbl',
        'metode',
        'cp',
        'tps',
        'pemahaman_bermakna',
        'pertanyaan_pemantik',
        'pembukaan',
        'inti',
        'penutup',
        'asesmen',
        'pengayaan',
        'remedial',
        'refleksi',
        'referensi',
        'glosarium',
        'pustaka'
    ];

    private function atp() {
        return $this->belongsTo(Atp::class, 'atp_id', 'kode');
    }

    private function guru() {
        return $this->belongsTo(Guru::class, 'guru_id', 'nip');
    }

    private function rombel() {
        return $this->belongsTo(Rombel::class, 'rombel_id', 'kode');
    }
}
