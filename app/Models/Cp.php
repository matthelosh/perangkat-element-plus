<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cp extends Model
{
    use HasFactory;

    protected $fillable = [
        'kode',
        'fase',
        'elemen',
        'teks'
    ];

    function tps() {
        return $this->hasMany(Tp::class, 'cp_id', 'kode');
    }

    function atps() {
        return $this->hasMany(Atp::class, 'cp_id','kode');
    }
}
