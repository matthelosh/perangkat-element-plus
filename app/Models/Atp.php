<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Atp extends Model
{
    use HasFactory;
    protected $fillable = [
        'kode',
        'cp_id',
        'rombel_id',
        'guru_id',
        'kode',
        'materi',
        'tingkat',
        'tps',
        'aw',
        'semester',
        'asesmen',
        'urut'
    ];

    function ma() {
        return $this->hasOne(ModulAjar::class, 'atp_id', 'kode');
    }

    function cp() {
        return $this->belongsTo(Cp::class, 'cp_id', 'kode');
    }
}
