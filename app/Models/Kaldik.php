<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kaldik extends Model
{
    use HasFactory;

    protected $fillable = [
        'name', 
        'description',
        'start',
        'end',
        'location',
        'color',
        'user_id',
        'is_libur',
        'is_active'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
