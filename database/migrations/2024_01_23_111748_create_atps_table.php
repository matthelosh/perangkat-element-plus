<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('atps', function (Blueprint $table) {
            $table->id();
            $table->string('cp_id', 20);
            $table->string('rombel_id', 30)->nullable();
            $table->string('guru_id', 30)->nullable();
            $table->string('kode',100)->unique();
            $table->text('tps');
            $table->string('materi', 191);
            $table->string('tingkat', 10);
            $table->string('aw', 10);
            $table->string('semester', 10);
            $table->text('asesmen');
            $table->string('urut', 5);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('atps');
    }
};
