<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('modul_ajars', function (Blueprint $table) {
            $table->id();
            $table->string('kode', 30);
            $table->string('tingkat', 2);
            $table->string('atp_id', 60);
            $table->string('rombel_id', 50)->nullable();
            $table->string('guru_id', 50)->nullable();
            $table->date('tanggal')->nullable();
            $table->string('fase', 1)->nullable();
            $table->text('materi')->nullable();
            $table->text('kompetensi_awal')->nullable();
            $table->string('p5', 191)->nullable();
            $table->string('sarpras', 191)->nullable();
            $table->string('target_siswa', 191)->default('Siswa Reguler');
            $table->string('model_pbl', 100)->nullable();
            $table->string('metode', 191)->nullable();
            $table->text('cp')->nullable();
            $table->text('tps')->nullable();
            $table->text('pemahaman_bermakna')->nullable();
            $table->text('pertanyaan_pemantik')->nullable();
            $table->text('pembukaan')->nullable();
            $table->text('inti')->nullable();
            $table->text('penutup')->nullable();
            $table->text('asesmen')->nullable();
            $table->text('pengayaan')->nullable();
            $table->text('remedial')->nullable();
            $table->text('refleksi')->nullable();
            $table->text('referensi')->nullable();
            $table->text('glosarium')->nullable();
            $table->text('pustaka')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('modul_ajars');
    }
};
