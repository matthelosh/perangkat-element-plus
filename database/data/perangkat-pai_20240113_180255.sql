-- Valentina Studio --
-- MySQL dump --
-- ---------------------------------------------------------


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
-- ---------------------------------------------------------


-- Dump data of "cps" --------------------------------------
BEGIN;

INSERT INTO `cps`(`id`,`fase`,`elemen`,`teks`,`created_at`,`updated_at`,`kode`) VALUES 
( '1', 'A', 'Al-Quran dan
Hadis', 'Pendidikan Agama Islam dan Budi Pekerti menekankan kemampuan mengenal huruf hijaiah dan harakatnya, huruf hijaiah bersambung, dan kemampuan membaca surah-surah pendek Al-Qur�an dengan baik', NULL, NULL, 'qh-a' ),
( '2', 'A', 'Akidah', 'Peserta didik mengenal rukun iman kepada Allah melalui nama-namanya yang agung (asmaulhusna) dan mengenal para malaikat dan tugas yang diembannya.', NULL, NULL, 'aq-a' ),
( '3', 'A', 'Akhlak', 'Peserta didik terbiasa mempraktikkan nilai-nilai baik dalam kehidupan sehari-hari dalam ungkapan-ungkapan positif baik untuk dirinya maupun sesama manusia, terutama orang tua
dan guru. Peserta didik juga memahami pentingnya tradisi memberi dalam ajaran agama Islam. Mereka mulai mengenal norma yang ada di lingkungan sekitarnya. Peserta didik juga
terbiasa percaya diri mengungkapkan pendapat pribadinya dan belajar menghargai pendapat yang berbeda. Peserta didik juga terbiasa melaksanakan tugas kelompok serta memahami
pentingnya mengenali kekurangan diri dan kelebihan temannya demi terwujudnya suasana saling mendukung satu sama lain.', NULL, NULL, 'ak-a' ),
( '4', 'A', 'Fikih', 'Peserta didik mampu mengenal rukun Islam dan kalimah syahadatain, menerapkan tata cara bersuci, salat fardu, azan, ikamah, zikir dan berdoa setelah salat.', NULL, NULL, 'fq-a' ),
( '5', 'A', 'Sejarah Peradaban
Islam', 'Peserta didik mampu menceritakan secara sederhana kisah beberapa nabi yang wajib diimani.', NULL, NULL, 'sp-a' ),
( '6', 'B', 'Al-Quran dan
Hadis', 'Peserta didik mampu membaca surah-surah pendek atau ayat Al-Qur�an dan menjelaskan pesan pokoknya dengan baik. Peserta didik mengenal hadis tentang kewajiban salat dan menjaga hubungan baik dengan sesama serta mampu menerapkan dalam kehidupan seharihari.', NULL, NULL, 'qh-b' ),
( '7', 'B', 'Akidah', 'Peserta didik memahami sifat-sifat bagi Allah, beberapa asmaulhusna, mengenal kitab-kitab Allah, para nabi dan rasul Allah yang wajib diimani', NULL, NULL, 'aq-b' ),
( '8', 'B', 'Akhlak', 'Pada elemen akhlak, peserta didik menghormati dan berbakti kepada orang tua dan guru, dan menyampaikan ungkapan-ungkapan positif (kalimah ?ayyibah) dalam keseharian. Peserta didik memahami arti keragaman sebagai sebuah ketentuan dari Allah Swt. (sunnatull?h). Peserta didik mengenal norma yang ada di lingkungan sekitarnya dan lingkungan yang lebih luas, percaya diri mengungkapkan pendapat pribadi, memahami pentingnya musyawarah untuk mencapai kesepakatan dan pentingnya persatuan.', NULL, NULL, 'ak-b' ),
( '9', 'B', 'Fikih', 'Pada elemen fikih, peserta didik dapat melaksanakan puasa, salat jumat dan salat sunah dengan baik, memahami konsep balig dan tanggung jawab yang menyertainya (takl?f)', NULL, NULL, 'fq-b' ),
( '10', 'B', 'Sejarah Peradaban
Islam', 'Dalam pemahamannya tentang sejarah, peserta didik mampu menceritakan kondisi Arab pra Islam, masa kanak-kanak dan remaja Nabi Muhammad saw. hingga diutus menjadi rasul, berdakwah, hijrah dan membangun Kota Madinah.', NULL, NULL, 'sp-b' ),
( '11', 'C', 'Al-Quran dan
Hadis', 'Peserta didik mampu membaca, menghafal, menulis, dan memahami pesan pokok surahsurah pendek dan ayat Al-Qur�an tentang keragaman dengan baik dan benar', NULL, NULL, 'qh-c' ),
( '12', 'C', 'Akidah', 'Peserta didik dapat mengenal Allah melalui asmaulhusna, memahami keniscayaan peritiwa hari akhir, qad?? dan qadr', NULL, NULL, 'aq-c' ),
( '13', 'C', 'Akhlak', 'Peserta didik mengenal dialog antar agama dan kepercayaan dan menyadari peluang dan tantangan yang bisa muncul dari keragaman di Indonesia. Peserta didik memahami arti ideologi secara sederhana dan pandangan hidup dan memahami pentingnya menjaga kesatuan atas keberagaman. Peserta didik juga memahami pentingnya introspeksi diri untuk menjadi pribadi yang lebih baik setiap harinya. Peserta didik memahami pentingnya pendapat yang logis, menerima perbedaan pendapat, dan menemukan titik kesamaan (kalimah saw??) untuk mewujudkan persatuan dan kerukunan. Peserta didik memahami peran manusia sebagai khalifah Allah di bumi untuk menebarkan kasih sayang dan tidak membuat kerusakan di muka bumi.', NULL, NULL, 'ak-c' ),
( '14', 'C', 'Fikih', 'Pada elemen fikih, peserta didik mampu memahami zakat, infak, sedekah dan hadiah, memahami ketentuan haji, halal dan haram serta mempraktikkan puasa sunnah', NULL, NULL, 'fq-c' ),
( '15', 'C', 'Sejarah Peradaban
Islam', 'Pada elemen sejarah, peserta didik menghayati ibrah dari kisah Nabi Muhammad saw. di masa separuh akhir kerasulannya serta kisah alkhulaf?al-r?syid?n.', NULL, NULL, 'sp-c' );
COMMIT;
-- ---------------------------------------------------------


-- Dump data of "failed_jobs" ------------------------------
-- ---------------------------------------------------------


-- Dump data of "gurus" ------------------------------------
BEGIN;

INSERT INTO `gurus`(`id`,`nip`,`nuptk`,`fullname`,`jk`,`hp`,`alamat`,`status`,`sertifikasi`,`sekolah_id`,`created_at`,`updated_at`) VALUES 
( '1', '001', NULL, 'Bejo Kumayangan', 'Laki-laki', '6285173303784', 'Malang', 'gtt', '0', NULL, NULL, NULL ),
( '2', '198407032019031007', NULL, 'Muhammad Soleh, S. Pd.I', 'Laki-laki', '6285954944407', 'Petungsewu RT.14 RW.04', 'pns', '1', '20518848', '2024-01-06 09:07:50', '2024-01-06 09:08:27' ),
( '137', '6492bba9cc082', NULL, 'NUR FITRIANI, , S. Pd I', 'Perempuan', '6282231402209', 'Malang', 'pns', '1', '20518710', '2024-01-09 13:28:32', '2024-01-09 13:28:32' ),
( '138', '198406122010012021', NULL, 'ELMI YUNI A. M, S. Pd I', 'Perempuan', '6285604162677', 'Malang', 'pns', '1', '20518454', '2024-01-09 13:28:32', '2024-01-09 13:28:32' ),
( '139', '199108232019031018', NULL, 'M. HAMID HABIBI, S. Pd I', 'Laki-laki', '6281330668526', 'Malang', 'pns', '1', '20518384', '2024-01-09 13:28:32', '2024-01-09 13:28:32' ),
( '140', '6492bba9ce10d', NULL, 'IMROAH, S. Pd I', 'Perempuan', '6288235892805', 'Malang', 'pns', '1', '20517168', '2024-01-09 13:28:32', '2024-01-09 13:28:32' ),
( '141', '198912032019032015', NULL, 'CINDY PUSPITASARI, S. Pd I', 'Perempuan', '6281233666895', 'Malang', 'pns', '1', '20517114', '2024-01-09 13:28:32', '2024-01-09 13:28:32' ),
( '142', '199110142019032015', NULL, 'MUSFIROTUL HIDAYAH, S. Pd I', 'Perempuan', '6282257299126', 'Malang', 'pns', '1', '20517118', '2024-01-09 13:28:32', '2024-01-09 13:28:32' ),
( '143', '6492bba9cfae8', NULL, 'JUMAIN, S. Pd I', 'Laki-laki', '6281334118184', 'Malang', 'pns', '1', '20517295', '2024-01-09 13:28:32', '2024-01-09 13:28:32' ),
( '144', '6492bba9d0430', NULL, 'IKHWANUDDIN ACHMAD, S. Pd I', 'Laki-laki', '6282234614546', 'Malang', 'pns', '0', '20517277', '2024-01-09 13:28:32', '2024-01-09 13:28:32' ),
( '145', '196606202000032001', NULL, 'DEWI ROHANA, S. PdI', 'Perempuan', '6285100485506', 'Malang', 'pns', '1', '20516949', '2024-01-09 13:28:32', '2024-01-09 13:28:32' ),
( '146', '6492bba9d1108', NULL, 'MIFTAKHUL JANNAH, S. Pd I', 'Perempuan', '62816510399', 'Malang', 'pns', '0', '20516858', '2024-01-09 13:28:32', '2024-01-09 13:28:32' ),
( '147', '6492bba9d16ea', NULL, 'SILVIA AMALIYATUS. S , S. Pd I', 'Perempuan', '6285727894982', 'Malang', 'pns', '0', '20516859', '2024-01-09 13:28:32', '2024-01-09 13:28:32' ),
( '148', '6492bba9d1d92', NULL, 'YUSUF PRIMA, S. Pd I', 'Laki-laki', '6282233466045', 'Malang', 'pns', '0', '69772577', '2024-01-09 13:28:32', '2024-01-09 13:28:32' ),
( '149', '6492bba9d264b', NULL, 'RIZAL ULAA, S. Pd I', 'Laki-laki', '6281217311976', 'Malang', 'pns', '0', '20518471', '2024-01-09 13:28:32', '2024-01-09 13:28:32' ),
( '150', '6492bba9d2bd6', NULL, 'LESTYANING JULIAWATI , S. Pd I', 'Perempuan', '6282229252933', 'Malang', 'pns', '1', '20518385', '2024-01-09 13:28:32', '2024-01-09 13:28:32' ),
( '151', '197101262021212005', NULL, 'KUJAYANAH, S. PdI, M. Pd', 'Perempuan', '6282221000037', 'Malang', 'pns', '0', '20517169', '2024-01-09 13:28:32', '2024-01-09 13:28:32' ),
( '152', '6492bba9d358c', NULL, 'RETNO INDAH, S. Pd I', 'Perempuan', '6282333851515', 'Malang', 'pns', '0', '20517115', '2024-01-09 13:28:32', '2024-01-09 13:28:32' ),
( '153', '6492bba9d3ab0', NULL, 'KHOIRIYAH ULFA, S. Pd I', 'Perempuan', '6283834164817', 'Malang', 'pns', '0', '20517121', '2024-01-09 13:28:32', '2024-01-09 13:28:32' ),
( '154', '6492bba9d3f9c', NULL, 'SITI FATIMAH, S. PdI', 'Perempuan', '6285102536393', 'Malang', 'pns', '1', '20518723', '2024-01-09 13:28:32', '2024-01-09 13:28:32' ),
( '155', '197404182000122003', NULL, 'SITI KHOTIMAH , S. Pd I', 'Perempuan', '6285234412074', 'Malang', 'pns', '1', '20518723', '2024-01-09 13:28:32', '2024-01-09 13:28:32' ),
( '156', '197601152005011002', NULL, 'CHOIRI MAHMUDI, M. Ag', 'Laki-laki', '6281555821010', 'Malang', 'pns', '1', '20517278', '2024-01-09 13:28:32', '2024-01-09 13:28:32' ),
( '157', '6492bba9d500d', NULL, 'NILA NUR FAIZA, S. Pd I', 'Perempuan', '6282140664420', 'Malang', 'pns', '0', '20516950', '2024-01-09 13:28:32', '2024-01-09 13:28:32' ),
( '158', '196605062007011018', NULL, 'ABDUL MANAB, Drs.', 'Laki-laki', '6281217636911', 'Malang', 'pns', '1', '20516845', '2024-01-09 13:28:32', '2024-01-09 13:28:32' ),
( '159', '6492bba9d5ca0', NULL, 'SUGENG HARYANTO, S. Pd I', 'Laki-laki', '6285252133475', 'Malang', 'pns', '1', '20516860', '2024-01-09 13:28:32', '2024-01-09 13:28:32' ),
( '160', '198002082010012010', NULL, 'DIAH MUNINGGAR, S. Pd I', 'Perempuan', '6282230604431', 'Malang', 'pns', '1', '20516971', '2024-01-09 13:28:32', '2024-01-09 13:28:32' ),
( '161', '6492bba9d6db6', NULL, 'WAWAN NANANG QOSIM, S.Pd.', 'Laki-laki', '6285842637934', 'Malang', 'pns', '0', '20518850', '2024-01-09 13:28:32', '2024-01-09 13:28:32' ),
( '162', '197609042008011013', NULL, 'HASAN LUTFI, S. Pd I, M. Pd', 'Laki-laki', '6282142934199', 'Malang', 'pns', '1', '20518850', '2024-01-09 13:28:32', '2024-01-09 13:28:32' ),
( '163', '6492bba9d7dfd', NULL, 'ALFAN SUUDI, S. Pd I', 'Laki-laki', '6282333066580', 'Malang', 'pns', '0', '20518472', '2024-01-09 13:28:32', '2024-01-09 13:28:32' ),
( '164', '6492bba9d8591', NULL, 'SAIFUL ARIF, M. Pd', 'Laki-laki', '6281510897827', 'Malang', 'pns', '1', '20518386', '2024-01-09 13:28:32', '2024-01-09 13:28:32' ),
( '165', '6492bba9d8d88', NULL, 'MUTOMMIMAH, S. Pd I', 'Perempuan', '6281232236234', 'Malang', 'pns', '1', '20517170', '2024-01-09 13:28:32', '2024-01-09 13:28:32' ),
( '166', '197607112005012003', NULL, 'HULLATIN, M. P d I', 'Perempuan', '6287865227611', 'Malang', 'pns', '1', '20517142', '2024-01-09 13:28:32', '2024-01-09 13:28:32' ),
( '167', '6492bba9d9b03', NULL, 'ROMDHON, S. Pd I', 'Laki-laki', '6285730018149', 'Malang', 'pns', '0', '20516951', '2024-01-09 13:28:32', '2024-01-09 13:28:32' ),
( '168', '6492bba9da31e', NULL, 'SITI KHUTOBAH, S. Pd I', 'Perempuan', '6285646797330', 'Malang', 'pns', '0', '20516846', '2024-01-09 13:28:32', '2024-01-09 13:28:32' ),
( '169', '198204032010012020', NULL, 'LASIATUN, S. Pd I', 'Perempuan', '6285335233335', 'Malang', 'pns', '1', '20518851', '2024-01-09 13:28:32', '2024-01-09 13:28:32' ),
( '170', '196405031987032012', NULL, 'LILIK SUMROTUL CHOIROH, S. Pd I', 'Perempuan', '6285100478408', 'Malang', 'pns', '1', '20516847', '2024-01-09 13:28:32', '2024-01-09 13:28:32' ),
( '171', '196212081986032009', NULL, 'MUDAWAMAH, S. Pd I', 'Perempuan', '6282142954975', 'Malang', 'pns', '1', '20516973', '2024-01-09 13:28:32', '2024-01-09 13:28:32' ),
( '172', '6492bba9ddd8c', NULL, 'AHMAD BAYIDI, S. PdI.', 'Laki-laki', '-', 'Malang', 'pns', '0', '20518385', '2024-01-09 13:28:32', '2024-01-09 13:28:32' );
COMMIT;
-- ---------------------------------------------------------


-- Dump data of "jadwals" ----------------------------------
BEGIN;

INSERT INTO `jadwals`(`id`,`sekolah_id`,`rombel_id`,`guru_id`,`jamke`,`hari`,`created_at`,`updated_at`) VALUES 
( '1', '20518848', '23242-20518848-6', '2', '1', 'Senin', '2024-01-06 10:38:51', '2024-01-06 10:38:51' ),
( '2', '20518848', '23242-20518848-6', '2', '2', 'Senin', '2024-01-06 10:38:54', '2024-01-06 10:38:54' ),
( '3', '20518848', '23242-20518848-6', '2', '3', 'Senin', '2024-01-06 10:38:56', '2024-01-06 10:38:56' ),
( '4', '20518848', '23242-20518848-6', '2', '4', 'Senin', '2024-01-06 10:38:59', '2024-01-06 10:38:59' ),
( '5', '20518848', '23242-20518848-1', '2', '1', 'Selasa', '2024-01-06 12:44:12', '2024-01-06 12:44:12' ),
( '6', '20518848', '23242-20518848-1', '2', '2', 'Selasa', '2024-01-06 12:44:18', '2024-01-06 12:44:18' ),
( '7', '20518848', '23242-20518848-1', '2', '3', 'Selasa', '2024-01-06 12:44:21', '2024-01-06 12:44:21' ),
( '8', '20518848', '23242-20518848-1', '2', '4', 'Selasa', '2024-01-06 12:44:23', '2024-01-06 12:44:23' ),
( '9', '20518848', '23242-20518848-3', '2', '5', 'Selasa', '2024-01-06 12:44:31', '2024-01-06 12:44:31' ),
( '10', '20518848', '23242-20518848-3', '2', '6', 'Selasa', '2024-01-06 12:44:34', '2024-01-06 12:44:34' ),
( '11', '20518848', '23242-20518848-3', '2', '7', 'Selasa', '2024-01-06 12:44:38', '2024-01-06 12:44:38' ),
( '12', '20518848', '23242-20518848-3', '2', '8', 'Selasa', '2024-01-06 12:44:43', '2024-01-06 12:44:43' ),
( '13', '20518848', '23242-20518848-2', '2', '1', 'Rabu', '2024-01-06 12:44:49', '2024-01-06 12:44:49' ),
( '14', '20518848', '23242-20518848-2', '2', '2', 'Rabu', '2024-01-06 12:44:53', '2024-01-06 12:44:53' ),
( '15', '20518848', '23242-20518848-2', '2', '3', 'Rabu', '2024-01-06 12:44:56', '2024-01-06 12:44:56' ),
( '16', '20518848', '23242-20518848-2', '2', '4', 'Rabu', '2024-01-06 12:44:59', '2024-01-06 12:44:59' ),
( '17', '20518848', '23242-20518848-4', '2', '1', 'Kamis', '2024-01-06 12:45:04', '2024-01-06 12:45:04' ),
( '18', '20518848', '23242-20518848-4', '2', '2', 'Kamis', '2024-01-06 12:45:07', '2024-01-06 12:45:07' ),
( '19', '20518848', '23242-20518848-4', '2', '3', 'Kamis', '2024-01-06 12:45:10', '2024-01-06 12:45:10' ),
( '20', '20518848', '23242-20518848-4', '2', '4', 'Kamis', '2024-01-06 12:45:13', '2024-01-06 12:45:13' ),
( '21', '20518848', '23242-20518848-5', '2', '1', 'Jumat', '2024-01-06 12:45:18', '2024-01-06 12:45:18' ),
( '22', '20518848', '23242-20518848-5', '2', '2', 'Jumat', '2024-01-06 12:45:22', '2024-01-06 12:45:22' ),
( '23', '20518848', '23242-20518848-5', '2', '3', 'Jumat', '2024-01-06 12:45:26', '2024-01-06 12:45:26' ),
( '24', '20518848', '23242-20518848-5', '2', '4', 'Jumat', '2024-01-06 12:45:29', '2024-01-06 12:45:29' );
COMMIT;
-- ---------------------------------------------------------


-- Dump data of "kaldiks" ----------------------------------
BEGIN;

INSERT INTO `kaldiks`(`id`,`name`,`description`,`start`,`end`,`location`,`color`,`user_id`,`is_libur`,`is_active`,`created_at`,`updated_at`) VALUES 
( '1', 'LHB', 'Tahun Baru 2024', '2024-01-01', '2024-01-01', NULL, 'error', 'matsoleh', '1', '1', '2024-01-06 10:28:57', '2024-01-06 10:28:57' ),
( '2', 'Imlek', 'Hari Raya Imlek', '2024-01-22', '2024-01-22', NULL, 'error', 'matsoleh', '1', '1', '2024-01-06 10:30:16', '2024-01-06 10:30:16' ),
( '3', 'Isro\' Mi\'roj', 'Peringatan Isro\' Mi\'roj Nabi Muhammad SAW', '2024-02-08', '2024-02-08', NULL, 'teal', 'matsoleh', '1', '1', '2024-01-06 10:31:20', '2024-01-06 10:31:20' ),
( '4', 'Cuti Bersama', 'Libur Cuti Bersama', '2024-02-09', '2024-02-10', NULL, 'error', 'matsoleh', '1', '1', '2024-01-06 10:31:48', '2024-01-06 10:31:48' ),
( '5', 'Nyepi', 'Hari Raya Nyepi', '2024-03-11', '2024-03-11', NULL, 'error', 'matsoleh', '1', '1', '2024-01-06 10:32:27', '2024-01-06 10:32:27' ),
( '6', 'Cuti Bersama', 'Cuti Bersama', '2024-03-12', '2024-03-12', NULL, 'error', 'matsoleh', '1', '1', '2024-01-06 10:32:43', '2024-01-06 10:32:43' ),
( '7', 'Idul Fitri', 'Libur Hari Raya Idul Fitri', '2024-04-08', '2024-04-13', NULL, 'teal', 'matsoleh', '1', '1', '2024-01-06 10:34:35', '2024-01-06 10:34:35' ),
( '8', 'Cuti Bersama', 'Cuti Bersama Idul Fitri', '2024-04-15', '2024-04-15', NULL, 'error', 'matsoleh', '1', '1', '2024-01-06 10:35:02', '2024-01-06 10:35:02' ),
( '9', 'Hari Buruh Nasional', 'Hari Buruh', '2024-05-01', '2024-05-01', NULL, 'error', 'matsoleh', '1', '1', '2024-01-06 10:35:44', '2024-01-06 10:35:44' ),
( '10', 'Kenaikan Isa Al Masih', 'Kenaikan Isa Al Masih', '2024-05-08', '2024-05-08', NULL, 'error', 'matsoleh', '1', '1', '2024-01-06 10:36:25', '2024-01-06 10:36:25' ),
( '11', 'Cuti Bersama', 'Cuti Bersama', '2024-05-09', '2024-05-09', NULL, 'error', 'matsoleh', '1', '1', '2024-01-06 10:36:37', '2024-01-06 10:36:37' ),
( '12', 'Waisak', 'Hari Raya Waisak', '2024-05-22', '2024-05-22', NULL, 'error', 'matsoleh', '1', '1', '2024-01-06 10:36:59', '2024-01-06 10:36:59' ),
( '13', 'Cuti Bersama', 'Cuti Bersama', '2024-05-23', '2024-05-23', NULL, 'error', 'matsoleh', '1', '1', '2024-01-06 10:37:12', '2024-01-06 10:37:12' ),
( '14', 'Hari Lahir Pancasila', 'Hari Lahir Pancasila', '2024-06-01', '2024-06-01', NULL, 'error', 'matsoleh', '1', '1', '2024-01-06 10:37:44', '2024-01-06 10:37:44' ),
( '15', 'Idul Adha', 'Hari Raya Idul Adha', '2024-06-17', '2024-06-17', NULL, 'teal', 'matsoleh', '1', '1', '2024-01-06 10:38:06', '2024-01-06 10:38:06' ),
( '16', 'Cuti Bersama', 'Cuti Bersama', '2024-06-18', '2024-06-18', NULL, 'error', 'matsoleh', '1', '1', '2024-01-06 10:38:19', '2024-01-06 10:38:19' );
COMMIT;
-- ---------------------------------------------------------


-- Dump data of "materis" ----------------------------------
BEGIN;

INSERT INTO `materis`(`id`,`kode`,`pembelajaran_id`,`label`,`description`,`created_at`,`updated_at`) VALUES 
( '1', 'qh-a-1-6-A', 'qh-a-1-6', 'Mengenal Harakat', NULL, NULL, NULL ),
( '2', 'qh-a-1-6-B', 'qh-a-1-6', 'Membaca Huruf Hijaiyah', NULL, NULL, NULL ),
( '3', 'qh-a-1-6-C', 'qh-a-1-6', 'Membaca Surah Al-Ikhlas', NULL, NULL, NULL ),
( '4', 'qh-a-1-6-D', 'qh-a-1-6', 'Pesan Pokok Surah Al-Ikhlas', NULL, NULL, NULL ),
( '5', 'qh-a-1-6-sum', 'qh-a-1-6', 'TES SUMATIF PELAJARAN 6', NULL, NULL, NULL ),
( '6', 'aq-a-1-7-A', 'aq-a-1-7', 'Mengenal Asmaul Husna', NULL, NULL, NULL ),
( '7', 'aq-a-1-7-B', 'aq-a-1-7', 'Ar-Rahman', NULL, NULL, NULL ),
( '8', 'aq-a-1-7-C', 'aq-a-1-7', 'Ar-Rahim', NULL, NULL, NULL ),
( '9', 'aq-a-1-7-D', 'aq-a-1-7', 'Kasih Sayang Nabi Muhammad saw', NULL, NULL, NULL ),
( '10', 'aq-a-1-7-sum', 'aq-a-1-7', 'TES SUMATIF PELAJARAN 7', NULL, NULL, NULL ),
( '11', 'ak-a-1-8-A', 'ak-a-1-8', 'Berterima Kasih kepada Sesama', NULL, NULL, NULL ),
( '12', 'ak-a-1-8-B', 'ak-a-1-8', 'Cara Berterima Kasih', NULL, NULL, NULL ),
( '13', 'ak-a-1-8-C', 'ak-a-1-8', 'Berperilaku Disiplin', NULL, NULL, NULL ),
( '14', 'ak-a-1-8-D', 'ak-a-1-8', 'Cara Berperilaku Disiplin', NULL, NULL, NULL ),
( '15', 'ak-a-1-8-sum', 'ak-a-1-8', 'TES SUMATIF PELAJARAN 8', NULL, NULL, NULL ),
( '16', 'fq-a-1-9-A', 'fq-a-1-9', 'Hidup Bersih', NULL, NULL, NULL ),
( '17', 'fq-a-1-9-B', 'fq-a-1-9', 'Bersuci', NULL, NULL, NULL ),
( '18', 'fq-a-1-9-C', 'fq-a-1-9', 'Berwudu', NULL, NULL, NULL ),
( '19', 'fq-a-1-9-D', 'fq-a-1-9', 'Tayamum', NULL, NULL, NULL ),
( '20', 'fq-a-1-9-sum', 'fq-a-1-9', 'TES SUMATIF PELAJARAN 9', NULL, NULL, NULL ),
( '21', 'sp-a-1-10-A', 'sp-a-1-10', 'Allah swt. Menciptakan Nabi Adam a.s.', NULL, NULL, NULL ),
( '22', 'sp-a-1-10-B', 'sp-a-1-10', 'Nabi Adam a.s. dan Hawa Tinggal di Surga', NULL, NULL, NULL ),
( '23', 'sp-a-1-10-C', 'sp-a-1-10', 'Nabi Adam a.s. dan Hawa Diturunkan ke Bumi', NULL, NULL, NULL ),
( '24', 'sp-a-1-10-D', 'sp-a-1-10', 'Keteladanan Nabi Adam a.s.', NULL, NULL, NULL ),
( '25', 'sp-a-1-10-sum', 'sp-a-1-10', 'TES SUMATIF PELAJARAN 10', NULL, NULL, NULL ),
( '26', 'qh-a-2-6-A', 'qh-a-2-6', 'Senang Bisa Membaca Surah Al-Falaq', NULL, NULL, NULL ),
( '27', 'qh-a-2-6-B', 'qh-a-2-6', 'Senang Bisa Membaca Surah Al-Kautsar', NULL, NULL, NULL ),
( '28', 'qh-a-2-6-sum', 'qh-a-2-6', 'TES SUMATIF PELAJARAN 6', NULL, NULL, NULL ),
( '29', 'aq-a-2-7-A', 'aq-a-2-7', 'Siapakah Malaikat itu?', NULL, NULL, NULL ),
( '30', 'aq-a-2-7-B', 'aq-a-2-7', 'Pengertian Beriman Kepada Malaikat', NULL, NULL, NULL ),
( '31', 'aq-a-2-7-C', 'aq-a-2-7', 'Nama-Nama Malaikat Beserta Tugasnya', NULL, NULL, NULL ),
( '32', 'aq-a-2-7-D', 'aq-a-2-7', 'Ciri-Ciri Anak yang Beriman Kepada Malaikat', NULL, NULL, NULL ),
( '33', 'aq-a-2-7-sum', 'aq-a-2-7', 'TES SUMATIF PELAJARAN 7', NULL, NULL, NULL ),
( '34', 'ak-a-2-8-A', 'ak-a-2-8', 'Gaya Hidup Bersih', NULL, NULL, NULL ),
( '35', 'ak-a-2-8-B', 'ak-a-2-8', 'Gaya Hidup Rapi', NULL, NULL, NULL ),
( '36', 'ak-a-2-8-C', 'ak-a-2-8', 'Gaya Hidup Teratur', NULL, NULL, NULL ),
( '37', 'ak-a-2-8-sum', 'ak-a-2-8', 'TES SUMATIF PELAJARAN 8', NULL, NULL, NULL ),
( '38', 'fq-a-2-9-A', 'fq-a-2-9', 'Dzikir Setelah Shalat', NULL, NULL, NULL ),
( '39', 'fq-a-2-9-B', 'fq-a-2-9', 'Doa Setelah Shalat', NULL, NULL, NULL ),
( '40', 'fq-a-2-9-sum', 'fq-a-2-9', 'TES SUMATIF PELAJARAN 9', NULL, NULL, NULL ),
( '41', 'sp-a-2-10-A', 'sp-a-2-10', 'Nabi Ibrahim a.s. Ayah Para Nabi', NULL, NULL, NULL ),
( '42', 'sp-a-2-10-B', 'sp-a-2-10', 'Nabi Ibrahim Pada Masa Kanak-Kanak', NULL, NULL, NULL ),
( '43', 'sp-a-2-10-C', 'sp-a-2-10', 'Dakwah Nabi Ibrahim a.s.', NULL, NULL, NULL ),
( '44', 'sp-a-2-10-D', 'sp-a-2-10', 'Sikap Teladan Nabi Ibrahim a.s.', NULL, NULL, NULL ),
( '45', 'sp-a-2-10-E', 'sp-a-2-10', 'Meneladani Nabi Ibrahim a.s.', NULL, NULL, NULL ),
( '46', 'sp-a-2-10-sum', 'sp-a-2-10', 'TES SUMATIF PELAJARAN 10', NULL, NULL, NULL ),
( '47', 'qh-b-4-6-A', 'qh-b-4-6', 'Membaca Q.S. At-Tin', NULL, NULL, NULL ),
( '48', 'qh-b-4-6-B', 'qh-b-4-6', 'Memahami Pesan Pokok Q.S. At-Tin', NULL, NULL, NULL ),
( '49', 'qh-b-4-6-C', 'qh-b-4-6', 'Menulis Q.S. At-Tin', NULL, NULL, NULL ),
( '50', 'qh-b-4-6-D', 'qh-b-4-6', 'Menghafal Q.S. At-Tin', NULL, NULL, NULL ),
( '51', 'qh-b-4-6-E', 'qh-b-4-6', 'Hadits tentang Sialturahmi', NULL, NULL, NULL ),
( '52', 'qh-b-4-6-sum', 'qh-b-4-6', 'TES SUMATIF PELAJARAN 6', NULL, NULL, NULL ),
( '53', 'aq-b-4-7-A', 'aq-b-4-7', 'Makna Iman Kepada Rasul-Rasul Allah', NULL, NULL, NULL ),
( '54', 'aq-b-4-7-B', 'aq-b-4-7', 'Sifat-Sifat Rasul', NULL, NULL, NULL ),
( '55', 'aq-b-4-7-C', 'aq-b-4-7', 'Tujuan Diutusnya Rasul', NULL, NULL, NULL ),
( '56', 'aq-b-4-7-sum', 'aq-b-4-7', 'TES SUMATIF PELAJARAN 7', NULL, NULL, NULL ),
( '57', 'ak-b-4-8-A', 'ak-b-4-8', 'Salam', NULL, NULL, NULL ),
( '58', 'ak-b-4-8-B', 'ak-b-4-8', 'Senang Menolong Orang Lain', NULL, NULL, NULL ),
( '59', 'ak-b-4-8-C', 'ak-b-4-8', 'Ciri-Ciri Munafik', NULL, NULL, NULL ),
( '60', 'ak-b-4-8-sum', 'ak-b-4-8', 'TES SUMATIF PELAJARAN 8', NULL, NULL, NULL ),
( '61', 'fq-b-4-9-A', 'fq-b-4-9', 'Shalat Jumat', NULL, NULL, NULL ),
( '62', 'fq-b-4-9-B', 'fq-b-4-9', 'Shalat Duha', NULL, NULL, NULL ),
( '63', 'fq-b-4-9-C', 'fq-b-4-9', 'Shalat Tahajud', NULL, NULL, NULL ),
( '64', 'fq-b-4-9-sum', 'fq-b-4-9', 'TES SUMATIF PELAJARAN 9', NULL, NULL, NULL ),
( '65', 'sp-b-4-10-A', 'sp-b-4-10', 'Membangun Masjid', NULL, NULL, NULL ),
( '66', 'sp-b-4-10-B', 'sp-b-4-10', 'Menjalin Ukhuwah', NULL, NULL, NULL ),
( '67', 'sp-b-4-10-C', 'sp-b-4-10', 'Menggalang Kerukunan', NULL, NULL, NULL ),
( '68', 'sp-b-4-10-sum', 'sp-b-4-10', 'TES SUMATIF PELAJARAN 10', NULL, NULL, NULL ),
( '69', 'qh-c-5-6-A', 'qh-c-5-6', 'Membaca Al-Qur�an Surah Ali �Imr?n/3: 64 dan al-Baqarah/2: 256', NULL, NULL, NULL ),
( '70', 'qh-c-5-6-B', 'qh-c-5-6', 'Menulis Surah Ali-�Imr?n/3: 64 dan Al-Baqarah/2: 256', NULL, NULL, NULL ),
( '71', 'qh-c-5-6-C', 'qh-c-5-6', 'Mengartikan Surah Ali-�Imr?n/3: 64 dan Al-Baqarah/2: 256', NULL, NULL, NULL ),
( '72', 'qh-c-5-6-D', 'qh-c-5-6', 'Pesan Pokok Surah Q.S. Ali �Imr?n/3: 64 dan AlBaqarah/2: 256', NULL, NULL, NULL ),
( '73', 'qh-c-5-6-E', 'qh-c-5-6', 'Menghafal Q.S. Ali �Imr?n/3: 64 dan Al-Baqarah/2: 256', NULL, NULL, NULL ),
( '74', 'Qh-c-5-6-sum', 'qh-c-5-6', 'TES SUMATIF PELAJARAN 6', NULL, NULL, NULL ),
( '75', 'aq-c-5-7-A', 'aq-c-5-7', 'Makna Hari Akhir', NULL, NULL, NULL ),
( '76', 'aq-c-5-7-B', 'aq-c-5-7', 'Kejadian Hari Akhir', NULL, NULL, NULL ),
( '77', 'aq-c-5-7-C', 'aq-c-5-7', 'Hikmah Beriman kepada Hari Akhir', NULL, NULL, NULL ),
( '78', 'aq-5-7-sum', 'aq-c-5-7', 'TES SUMATIF PELAJARAN 7', NULL, NULL, NULL ),
( '79', 'ak-c-5-8-A', 'ak-c-5-8', 'Persaudaraan dalam Islam', NULL, NULL, NULL ),
( '80', 'ak-c-5-8-B', 'ak-c-5-8', 'Berteman Tanpa Membedakan Agama', NULL, NULL, NULL ),
( '81', 'ak-c-5-8-C', 'ak-c-5-8', 'Hikmah Berteman Tanpa Membedakan Agama', NULL, NULL, NULL ),
( '82', 'ak-c-5-8-sum', 'ak-c-5-8', 'TES SUMATIF PELAJARAN 8', NULL, NULL, NULL ),
( '83', 'fq-c-5-9-A', 'fq-c-5-9', 'Ibadah Haji', NULL, NULL, NULL ),
( '84', 'fq-c-5-9-B', 'fq-c-5-9', 'Ibadah Qurban', NULL, NULL, NULL ),
( '85', 'fq-c-5-9-sum', 'fq-c-5-9', 'TES SUMATIF PELAJARAN 9', NULL, NULL, NULL ),
( '86', 'sp-c-5-10-A', 'sp-c-5-10', 'Abu Bakr As-Shidiq r.a.', NULL, NULL, NULL ),
( '87', 'sp-c-5-10-B', 'sp-c-5-10', 'Umar bin Khattab r.a.', NULL, NULL, NULL ),
( '88', 'sp-c-5-10-C', 'sp-c-5-10', 'Utsman bin �Affan r.a.', NULL, NULL, NULL ),
( '89', 'sp-c-5-10-D', 'sp-c-5-10', 'Ali bin Abu Thalib r.a.', NULL, NULL, NULL ),
( '90', 'sp-c-5-10-sum', 'sp-c-5-10', 'TES SUMATIF PELAJARAN 10', NULL, NULL, NULL );
COMMIT;
-- ---------------------------------------------------------


-- Dump data of "menus" ------------------------------------
BEGIN;

INSERT INTO `menus`(`id`,`label`,`url`,`icon`,`parent_id`,`roles`,`created_at`,`updated_at`) VALUES 
( '1', 'Beranda', '/beranda', 'mdi-home', '0', 'all', NULL, NULL ),
( '2', 'Utama', '#', 'mdi-database', '0', 'all', NULL, NULL ),
( '3', 'Data Sekolah', '/sekolah', 'mdi-city', '2', 'all', NULL, NULL ),
( '4', 'Guru', '/guru', 'mdi-account-tie', '2', 'admin', NULL, NULL ),
( '5', 'Rombel', '/rombel', 'mdi-google-classroom', '2', 'all', NULL, NULL ),
( '6', 'Siswa', '/siswa', 'mdi-human-child', '2', 'all', NULL, NULL ),
( '7', 'Periode', '/periode', 'mdi-calendar', '2', 'admin', NULL, NULL ),
( '8', 'Perangkat', '#', 'mdi-book', '0', 'gpai', NULL, NULL ),
( '9', 'Rencana', '/rencana', 'mdi-pencil', '8', 'gpai', NULL, NULL ),
( '10', 'Pelaksanaan', '/proses', 'mdi-reload', '8', 'gpai', '2024-01-10 07:00:00', '2024-01-10 07:00:00' ),
( '11', 'Kaldik', '/kaldik', 'mdi-calendar', '2', 'admin', '2024-01-01 07:00:00', '2024-01-01 07:00:00' ),
( '12', 'Evaluasi', '/evaluasi', 'mdi-chart-bar', '8', 'gpai', NULL, NULL ),
( '13', 'Pengaturan', '/setting', 'mdi-cog', '0', 'admin', '2024-01-10 07:00:00', '2024-01-10 07:00:00' );
COMMIT;
-- ---------------------------------------------------------


-- Dump data of "migrations" -------------------------------
BEGIN;

INSERT INTO `migrations`(`id`,`migration`,`batch`) VALUES 
( '1', '2014_10_12_000000_create_users_table', '1' ),
( '2', '2014_10_12_100000_create_password_reset_tokens_table', '1' ),
( '3', '2019_08_19_000000_create_failed_jobs_table', '1' ),
( '4', '2019_12_14_000001_create_personal_access_tokens_table', '1' ),
( '5', '2023_03_13_081435_create_menus_table', '1' ),
( '6', '2023_03_13_084731_create_sekolahs_table', '1' ),
( '7', '2023_03_21_124258_create_periodes_table', '1' ),
( '8', '2023_04_03_145441_create_gurus_table', '1' ),
( '9', '2023_04_04_020457_create_table_kaldik', '1' ),
( '10', '2023_04_06_100833_create_rombels_table', '1' ),
( '11', '2023_04_06_145108_create_siswas_table', '1' ),
( '13', '2023_04_10_083826_create_jadwals_table', '1' ),
( '16', '2024_01_06_093311_add_fase_to_rombel', '2' ),
( '17', '2024_01_06_123031_create_cps_table', '3' ),
( '18', '2024_01_06_125316_create_p5s_table', '4' ),
( '21', '2024_01_06_125634_create_pembelajarans_table', '5' ),
( '22', '2024_01_06_134311_create_materis_table', '5' ),
( '23', '2024_01_06_192943_create_protas_table', '6' ),
( '25', '2024_01_10_051059_add_kode_cps', '7' ),
( '26', '2024_01_11_022935_create_prosems_table', '8' );
COMMIT;
-- ---------------------------------------------------------


-- Dump data of "p5s" --------------------------------------
BEGIN;

INSERT INTO `p5s`(`id`,`profil`,`ciri`,`created_at`,`updated_at`) VALUES 
( '1', 'Beriman, bertakwa kepada Tuhan YME, dan berakhlak mulia', 'Pelajar Indonesia yang beriman, bertakwa kepada Tuhan YME, dan berakhlak mulia adalah pelajar yang berakhlak dalam hubungannya dengan Tuhan Yang Maha Esa. Ia memahami ajaran agama dan kepercayaannya serta menerapkan pemahaman tersebut dalam kehidupannya sehari-hari. Ada lima elemen kunci beriman, bertakwa kepada Tuhan YME, dan berakhlak mulia: (a) akhlak beragama (b) akhlak pribadi (c) akhlak kepada manusia (d) akhlak kepada alam dan (e) akhlak bernegara', NULL, NULL ),
( '2', 'Berkebinekaan global', 'Pelajar Indonesia mempertahankan budaya luhur, lokalitas dan identitasnya, dan tetap berpikiran terbuka dalam berinteraksi dengan budaya lain, sehingga menumbuhkan rasa saling menghargai dan kemungkinan terbentuknya dengan budaya luhur yang positif dan tidak bertentangan dengan budaya luhur bangsa. Elemen dan kunci kebinekaan global meliputi mengenal dan menghargai budaya, kemampuan komunikasi interkultural dalam berinteraksi dengan sesama, dan refleksi dan tanggung jawab terhadap pengalaman kebinekaan', NULL, NULL ),
( '3', 'Bergotong royong', 'Pelajar Indonesia memiliki kemampuan bergotong-royong, yaitu kemampuan untuk melakukan kegiatan secara bersama-sama dengan suka rela agar kegiatan yang dikerjakan dapat berjalan lancar, mudah dan ringan. Elemen-elemen dari bergotong royong adalah kolaborasi, kepedulian, dan berbagi', NULL, NULL ),
( '4', 'Mandiri', 'Pelajar Indonesia merupakan pelajar mandiri, yaitu pelajar yang bertanggung jawab atas proses dan hasil belajarnya. Elemen kunci dari mandiri terdiri dari kesadaran akan diri dan situasi yang dihadapi serta regulasi diri.', NULL, NULL ),
( '5', 'Bernalar kritis', 'Pelajar yang bernalar kritis mampu secara objektif memproses informasi baik kualitatif maupun kuantitatif, membangun keterkaitan antara berbagai informasi, menganalisis informasi, mengevaluasi dan menyimpulkannya. Elemen-elemen dari bernalar kritis adalah memperoleh dan memproses informasi dan gagasan, menganalisis dan mengevaluasi penalaran, merefleksi pemikiran dan proses berpikir, dan mengambil Keputusan.', NULL, NULL ),
( '6', 'Kreatif', 'Pelajar yang kreatif mampu memodifikasi dan menghasilkan sesuatu yang orisinal, bermakna, bermanfaat, dan berdampak. Elemen kunci dari kreatif terdiri dari menghasilkan gagasan yang orisinal serta menghasilkan karya dan tindakan yang orisinal', NULL, NULL );
COMMIT;
-- ---------------------------------------------------------


-- Dump data of "password_reset_tokens" --------------------
-- ---------------------------------------------------------


-- Dump data of "pembelajarans" ----------------------------
BEGIN;

INSERT INTO `pembelajarans`(`id`,`kode`,`tingkat`,`label`,`description`,`created_at`,`updated_at`) VALUES 
( '1', 'qh-a-1-6', '1', 'Al-Quran Pedoman Hidupku', NULL, NULL, NULL ),
( '2', 'aq-a-1-7', '1', 'Kasih Sayang Terhadap Sesama', NULL, NULL, NULL ),
( '3', 'ak-a-1-8', '1', 'Aku Suka Berterima Kasih dan Bersikap Disiplin', NULL, NULL, NULL ),
( '4', 'fq-a-1-9', '1', 'Membiasakan Hidup Bersih', NULL, NULL, NULL ),
( '5', 'sp-a-1-10', '1', 'Nabi Adam a.s. Manusia Pertama', NULL, NULL, NULL ),
( '6', 'qh-a-2-6', '2', 'Senang Bisa Membaca Al-Quran', NULL, NULL, NULL ),
( '7', 'aq-a-2-7', '2', 'Mari Mengenal Malaikat-Malaikat Allah', NULL, NULL, NULL ),
( '8', 'ak-a-2-8', '2', 'Aku Senang Bisa Berakhlak Terpuji', NULL, NULL, NULL ),
( '9', 'fq-a-2-9', '2', 'Ayo Dzikir dan Berdoa Setelah Shalat', NULL, NULL, NULL ),
( '10', 'sp-a-2-10', '2', 'Asyiknya Belajar Kisah Ayah Para Nabi', NULL, NULL, NULL ),
( '11', 'qh-b-4-6', '4', 'Mari Mengaji dan Mengkaji Q.S. At-Tin dan Hadits tentang Silaturahmi', NULL, NULL, NULL ),
( '12', 'aq-b-4-7', '4', 'Beriman Kepada Rasul-Rasul Allah', NULL, NULL, NULL ),
( '13', 'ak-b-4-8', '4', 'Aku Anak Saleh', NULL, NULL, NULL ),
( '14', 'fq-b-4-9', '4', 'Mengenal Shalat Jumat, Duha dan Tahajud', NULL, NULL, NULL ),
( '15', 'sp-b-4-10', '4', 'Kisah Nabi Muhammad saw.', NULL, NULL, NULL ),
( '16', 'qh-c-5-6', '5', 'Hidup Damai dalam Kebersamaan', NULL, NULL, NULL ),
( '17', 'aq-c-5-7', '5', 'Ketika Kehidupan Telah Berhenti', NULL, NULL, NULL ),
( '18', 'ak-c-5-8', '5', 'Senangnya Berteman', NULL, NULL, NULL ),
( '19', 'fq-c-5-9', '5', 'Ibadah Haji dan Kurban', NULL, NULL, NULL ),
( '20', 'sp-c-5-10', '5', 'Keteladanan Khulafaurrasyidin', NULL, NULL, NULL );
COMMIT;
-- ---------------------------------------------------------


-- Dump data of "periodes" ---------------------------------
BEGIN;

INSERT INTO `periodes`(`id`,`kode_periode`,`tapel`,`semester`,`deskripsi`,`start`,`end`,`is_active`,`created_at`,`updated_at`) VALUES 
( '1', '23242', '2023/2024', '2', 'Semester Genap 2023/2024', '2024-01-01', '2024-06-30', '1', '2024-01-06 09:14:07', '2024-01-06 09:14:28' ),
( '2', '24251', '2024/2025', '1', 'Semester Ganjil 2024/2025', '2024-07-01', '2024-12-31', '0', '2024-01-06 09:14:22', '2024-01-06 09:14:22' );
COMMIT;
-- ---------------------------------------------------------


-- Dump data of "personal_access_tokens" -------------------
-- ---------------------------------------------------------


-- Dump data of "prosems" ----------------------------------
BEGIN;

INSERT INTO `prosems`(`id`,`materi_id`,`guru_id`,`pembelajaran_id`,`rombel_id`,`tanggal`,`minggu_ke`,`hari`,`created_at`,`updated_at`) VALUES 
( '1', 'qh-a-1-6-A', '198407032019031007', 'qh-a-1-6', '23242-20518848-1', '2024-01-02', '1', 'Selasa', '2024-01-12 01:31:53', '2024-01-12 01:31:53' ),
( '2', 'qh-a-1-6-B', '198407032019031007', 'qh-a-1-6', '23242-20518848-1', '2024-01-09', '2', 'Selasa', '2024-01-12 01:31:53', '2024-01-12 01:31:53' ),
( '3', 'qh-a-1-6-C', '198407032019031007', 'qh-a-1-6', '23242-20518848-1', '2024-01-16', '3', 'Selasa', '2024-01-12 01:31:53', '2024-01-12 01:31:53' ),
( '4', 'qh-a-1-6-D', '198407032019031007', 'qh-a-1-6', '23242-20518848-1', '2024-01-23', '4', 'Selasa', '2024-01-12 01:31:53', '2024-01-12 01:31:53' ),
( '5', 'qh-a-1-6-sum', '198407032019031007', 'qh-a-1-6', '23242-20518848-1', '2024-01-30', '5', 'Selasa', '2024-01-12 01:31:53', '2024-01-12 01:31:53' ),
( '6', 'aq-a-1-7-A', '198407032019031007', 'aq-a-1-7', '23242-20518848-1', '2024-02-06', '6', 'Selasa', '2024-01-12 01:31:53', '2024-01-12 01:31:53' ),
( '7', 'aq-a-1-7-B', '198407032019031007', 'aq-a-1-7', '23242-20518848-1', '2024-02-13', '7', 'Selasa', '2024-01-12 01:31:53', '2024-01-12 01:31:53' ),
( '8', 'aq-a-1-7-C', '198407032019031007', 'aq-a-1-7', '23242-20518848-1', '2024-02-20', '8', 'Selasa', '2024-01-12 01:31:53', '2024-01-12 01:31:53' ),
( '9', 'aq-a-1-7-D', '198407032019031007', 'aq-a-1-7', '23242-20518848-1', '2024-02-27', '9', 'Selasa', '2024-01-12 01:31:53', '2024-01-12 01:31:53' ),
( '10', 'aq-a-1-7-sum', '198407032019031007', 'aq-a-1-7', '23242-20518848-1', '2024-03-05', '10', 'Selasa', '2024-01-12 01:31:53', '2024-01-12 01:31:53' ),
( '11', 'ak-a-1-8-A', '198407032019031007', 'ak-a-1-8', '23242-20518848-1', '2024-03-19', '12', 'Selasa', '2024-01-12 01:31:53', '2024-01-12 01:31:53' ),
( '12', 'ak-a-1-8-B', '198407032019031007', 'ak-a-1-8', '23242-20518848-1', '2024-03-26', '13', 'Selasa', '2024-01-12 01:31:53', '2024-01-12 01:31:53' ),
( '13', 'ak-a-1-8-C', '198407032019031007', 'ak-a-1-8', '23242-20518848-1', '2024-04-02', '14', 'Selasa', '2024-01-12 01:31:53', '2024-01-12 01:31:53' ),
( '14', 'ak-a-1-8-D', '198407032019031007', 'ak-a-1-8', '23242-20518848-1', '2024-04-16', '16', 'Selasa', '2024-01-12 01:31:53', '2024-01-12 01:31:53' ),
( '15', 'ak-a-1-8-sum', '198407032019031007', 'ak-a-1-8', '23242-20518848-1', '2024-04-23', '17', 'Selasa', '2024-01-12 01:31:53', '2024-01-12 01:31:53' ),
( '16', 'fq-a-1-9-A', '198407032019031007', 'fq-a-1-9', '23242-20518848-1', '2024-04-30', '18', 'Selasa', '2024-01-12 01:31:53', '2024-01-12 01:31:53' ),
( '17', 'fq-a-1-9-B', '198407032019031007', 'fq-a-1-9', '23242-20518848-1', '2024-05-07', '19', 'Selasa', '2024-01-12 01:31:53', '2024-01-12 01:31:53' ),
( '18', 'fq-a-1-9-C', '198407032019031007', 'fq-a-1-9', '23242-20518848-1', '2024-05-14', '20', 'Selasa', '2024-01-12 01:31:53', '2024-01-12 01:31:53' ),
( '19', 'fq-a-1-9-D', '198407032019031007', 'fq-a-1-9', '23242-20518848-1', '2024-05-21', '21', 'Selasa', '2024-01-12 01:31:53', '2024-01-12 01:31:53' ),
( '20', 'fq-a-1-9-sum', '198407032019031007', 'fq-a-1-9', '23242-20518848-1', '2024-05-28', '22', 'Selasa', '2024-01-12 01:31:53', '2024-01-12 01:31:53' ),
( '21', 'sp-a-1-10-A', '198407032019031007', 'sp-a-1-10', '23242-20518848-1', '2024-06-04', '23', 'Selasa', '2024-01-12 01:31:53', '2024-01-12 01:31:53' ),
( '22', 'sp-a-1-10-B', '198407032019031007', 'sp-a-1-10', '23242-20518848-1', '2024-06-04', '23', 'Selasa', '2024-01-12 01:31:53', '2024-01-12 01:31:53' ),
( '23', 'sp-a-1-10-C', '198407032019031007', 'sp-a-1-10', '23242-20518848-1', '2024-06-11', '24', 'Selasa', '2024-01-12 01:31:53', '2024-01-12 01:31:53' ),
( '24', 'sp-a-1-10-D', '198407032019031007', 'sp-a-1-10', '23242-20518848-1', '2024-06-18', '25', 'Selasa', '2024-01-12 01:31:53', '2024-01-12 01:31:53' ),
( '25', 'sp-a-1-10-sum', '198407032019031007', 'sp-a-1-10', '23242-20518848-1', '2024-06-25', '26', 'Selasa', '2024-01-12 01:31:53', '2024-01-12 01:31:53' );
COMMIT;
-- ---------------------------------------------------------


-- Dump data of "protas" -----------------------------------
BEGIN;

INSERT INTO `protas`(`id`,`rombel_id`,`pembelajaran_id`,`materi_id`,`aw`,`created_at`,`updated_at`) VALUES 
( '1', '23242-20518848-1', 'qh-a-1-6', 'qh-a-1-6-A', '4', '2024-01-10 04:58:31', '2024-01-10 04:58:31' ),
( '2', '23242-20518848-1', 'qh-a-1-6', 'qh-a-1-6-B', '4', '2024-01-10 04:58:31', '2024-01-10 04:58:31' ),
( '3', '23242-20518848-1', 'qh-a-1-6', 'qh-a-1-6-C', '4', '2024-01-10 04:58:31', '2024-01-10 04:58:31' ),
( '4', '23242-20518848-1', 'qh-a-1-6', 'qh-a-1-6-D', '4', '2024-01-10 04:58:31', '2024-01-10 04:58:31' ),
( '5', '23242-20518848-1', 'qh-a-1-6', 'qh-a-1-6-sum', '4', '2024-01-10 04:58:31', '2024-01-10 04:58:31' ),
( '6', '23242-20518848-1', 'aq-a-1-7', 'aq-a-1-7-A', '4', '2024-01-10 04:58:31', '2024-01-10 04:58:31' ),
( '7', '23242-20518848-1', 'aq-a-1-7', 'aq-a-1-7-B', '4', '2024-01-10 04:58:31', '2024-01-10 04:58:31' ),
( '8', '23242-20518848-1', 'aq-a-1-7', 'aq-a-1-7-C', '4', '2024-01-10 04:58:31', '2024-01-10 04:58:31' ),
( '9', '23242-20518848-1', 'aq-a-1-7', 'aq-a-1-7-D', '4', '2024-01-10 04:58:31', '2024-01-10 04:58:31' ),
( '10', '23242-20518848-1', 'aq-a-1-7', 'aq-a-1-7-sum', '4', '2024-01-10 04:58:31', '2024-01-10 04:58:31' ),
( '11', '23242-20518848-1', 'ak-a-1-8', 'ak-a-1-8-A', '4', '2024-01-10 04:58:31', '2024-01-10 04:58:31' ),
( '12', '23242-20518848-1', 'ak-a-1-8', 'ak-a-1-8-B', '4', '2024-01-10 04:58:31', '2024-01-10 04:58:31' ),
( '13', '23242-20518848-1', 'ak-a-1-8', 'ak-a-1-8-C', '4', '2024-01-10 04:58:31', '2024-01-10 04:58:31' ),
( '14', '23242-20518848-1', 'ak-a-1-8', 'ak-a-1-8-D', '4', '2024-01-10 04:58:31', '2024-01-10 04:58:31' ),
( '15', '23242-20518848-1', 'ak-a-1-8', 'ak-a-1-8-sum', '4', '2024-01-10 04:58:31', '2024-01-10 04:58:31' ),
( '16', '23242-20518848-1', 'fq-a-1-9', 'fq-a-1-9-A', '4', '2024-01-10 04:58:31', '2024-01-10 04:58:31' ),
( '17', '23242-20518848-1', 'fq-a-1-9', 'fq-a-1-9-B', '4', '2024-01-10 04:58:31', '2024-01-10 04:58:31' ),
( '18', '23242-20518848-1', 'fq-a-1-9', 'fq-a-1-9-C', '4', '2024-01-10 04:58:31', '2024-01-10 04:58:31' ),
( '19', '23242-20518848-1', 'fq-a-1-9', 'fq-a-1-9-D', '4', '2024-01-10 04:58:31', '2024-01-10 04:58:31' ),
( '20', '23242-20518848-1', 'fq-a-1-9', 'fq-a-1-9-sum', '4', '2024-01-10 04:58:31', '2024-01-10 04:58:31' ),
( '21', '23242-20518848-1', 'sp-a-1-10', 'sp-a-1-10-A', '4', '2024-01-10 04:58:31', '2024-01-10 04:58:31' ),
( '22', '23242-20518848-1', 'sp-a-1-10', 'sp-a-1-10-B', '4', '2024-01-10 04:58:31', '2024-01-10 04:58:31' ),
( '23', '23242-20518848-1', 'sp-a-1-10', 'sp-a-1-10-C', '4', '2024-01-10 04:58:31', '2024-01-10 04:58:31' ),
( '24', '23242-20518848-1', 'sp-a-1-10', 'sp-a-1-10-D', '4', '2024-01-10 04:58:31', '2024-01-10 04:58:31' ),
( '25', '23242-20518848-1', 'sp-a-1-10', 'sp-a-1-10-sum', '4', '2024-01-10 04:58:31', '2024-01-10 04:58:31' ),
( '26', '23242-20518848-4', 'qh-b-4-6', 'qh-b-4-6-A', '4', '2024-01-11 05:36:59', '2024-01-11 05:36:59' ),
( '27', '23242-20518848-4', 'qh-b-4-6', 'qh-b-4-6-B', '4', '2024-01-11 05:36:59', '2024-01-11 05:36:59' ),
( '28', '23242-20518848-4', 'qh-b-4-6', 'qh-b-4-6-C', '4', '2024-01-11 05:36:59', '2024-01-11 05:36:59' ),
( '29', '23242-20518848-4', 'qh-b-4-6', 'qh-b-4-6-D', '4', '2024-01-11 05:36:59', '2024-01-11 05:36:59' ),
( '30', '23242-20518848-4', 'qh-b-4-6', 'qh-b-4-6-E', '4', '2024-01-11 05:36:59', '2024-01-11 05:36:59' ),
( '31', '23242-20518848-4', 'qh-b-4-6', 'qh-b-4-6-sum', '4', '2024-01-11 05:36:59', '2024-01-11 05:36:59' ),
( '32', '23242-20518848-4', 'aq-b-4-7', 'aq-b-4-7-A', '4', '2024-01-11 05:36:59', '2024-01-11 05:36:59' ),
( '33', '23242-20518848-4', 'aq-b-4-7', 'aq-b-4-7-B', '4', '2024-01-11 05:36:59', '2024-01-11 05:36:59' ),
( '34', '23242-20518848-4', 'aq-b-4-7', 'aq-b-4-7-C', '4', '2024-01-11 05:36:59', '2024-01-11 05:36:59' ),
( '35', '23242-20518848-4', 'aq-b-4-7', 'aq-b-4-7-sum', '4', '2024-01-11 05:36:59', '2024-01-11 05:36:59' ),
( '36', '23242-20518848-4', 'ak-b-4-8', 'ak-b-4-8-A', '4', '2024-01-11 05:36:59', '2024-01-11 05:36:59' ),
( '37', '23242-20518848-4', 'ak-b-4-8', 'ak-b-4-8-B', '4', '2024-01-11 05:36:59', '2024-01-11 05:36:59' ),
( '38', '23242-20518848-4', 'ak-b-4-8', 'ak-b-4-8-C', '4', '2024-01-11 05:36:59', '2024-01-11 05:36:59' ),
( '39', '23242-20518848-4', 'ak-b-4-8', 'ak-b-4-8-sum', '4', '2024-01-11 05:36:59', '2024-01-11 05:36:59' ),
( '40', '23242-20518848-4', 'fq-b-4-9', 'fq-b-4-9-A', '4', '2024-01-11 05:36:59', '2024-01-11 05:36:59' ),
( '41', '23242-20518848-4', 'fq-b-4-9', 'fq-b-4-9-B', '4', '2024-01-11 05:36:59', '2024-01-11 05:36:59' ),
( '42', '23242-20518848-4', 'fq-b-4-9', 'fq-b-4-9-C', '4', '2024-01-11 05:36:59', '2024-01-11 05:36:59' ),
( '43', '23242-20518848-4', 'fq-b-4-9', 'fq-b-4-9-sum', '4', '2024-01-11 05:36:59', '2024-01-11 05:36:59' ),
( '44', '23242-20518848-4', 'sp-b-4-10', 'sp-b-4-10-A', '4', '2024-01-11 05:36:59', '2024-01-11 05:36:59' ),
( '45', '23242-20518848-4', 'sp-b-4-10', 'sp-b-4-10-B', '4', '2024-01-11 05:36:59', '2024-01-11 05:36:59' ),
( '46', '23242-20518848-4', 'sp-b-4-10', 'sp-b-4-10-C', '4', '2024-01-11 05:36:59', '2024-01-11 05:36:59' ),
( '47', '23242-20518848-4', 'sp-b-4-10', 'sp-b-4-10-sum', '4', '2024-01-11 05:36:59', '2024-01-11 05:36:59' );
COMMIT;
-- ---------------------------------------------------------


-- Dump data of "rombels" ----------------------------------
BEGIN;

INSERT INTO `rombels`(`id`,`kode_rombel`,`label`,`tingkat`,`sekolah_id`,`guru_id`,`periode_id`,`kurikulum`,`created_at`,`updated_at`,`fase`) VALUES 
( '1', '23242-20518848-1', 'I (SATU)', '1', '20518848', '2', '23242', 'merdeka', '2024-01-06 09:34:23', '2024-01-06 09:34:23', 'A' ),
( '2', '23242-20518848-2', 'II (DUA)', '2', '20518848', '2', '23242', 'merdeka', '2024-01-06 09:34:43', '2024-01-06 09:34:43', 'A' ),
( '3', '23242-20518848-3', 'III (TIGA)', '3', '20518848', '2', '23242', 'merdeka', '2024-01-06 09:34:56', '2024-01-06 09:34:56', 'B' ),
( '4', '23242-20518848-4', 'IV (EMPAT)', '4', '20518848', '2', '23242', 'merdeka', '2024-01-06 09:35:07', '2024-01-06 09:35:07', 'B' ),
( '5', '23242-20518848-5', 'V (LIMA)', '5', '20518848', '2', '23242', 'merdeka', '2024-01-06 09:35:30', '2024-01-06 09:35:30', 'C' ),
( '6', '23242-20518848-6', 'VI (ENAM)', '6', '20518848', '2', '23242', 'merdeka', '2024-01-06 09:36:31', '2024-01-06 09:36:31', 'C' ),
( '16', '23242-20518848-5a', 'V (LIMA) A', '5', '20518848', '2', '23242', 'merdeka', '2024-01-12 20:58:30', '2024-01-12 20:58:30', 'C' );
COMMIT;
-- ---------------------------------------------------------


-- Dump data of "sekolahs" ---------------------------------
BEGIN;

INSERT INTO `sekolahs`(`id`,`npsn`,`name`,`alamat`,`rt`,`rw`,`desa`,`kecamatan`,`kabupaten`,`kode_pos`,`telp`,`email`,`website`,`nama_ks`,`nip_ks`,`created_at`,`updated_at`) VALUES 
( '1', '20518710', 'SD I SUNAN AMPEL', 'JL. KATU NO 58 MENDALANKULON', '022', '008', 'MENDALANWANGI', 'WAGIR', 'MALANG', '65158', '0881026674264', 'sunanampelsdi@gmail.com', 'http://sdislamsunanampel.mysch.id', 'ETY SAROFAH', '-', '2024-01-06 09:04:55', '2024-01-06 09:04:55' ),
( '2', '20518848', 'SD NEGERI 1 BEDALISODO', 'JL. RAYA SENGON NO. 293', '008', '003', 'DALISODO', 'WAGIR', 'MALANG', '65158', '-', 'sdn.bedali01@gmail.com', 'https://sdn1-bedalisodo.sch.id', 'NURUL HUDA, S. PdSD', '196804222005011004', '2024-01-06 09:04:55', '2024-01-06 09:04:55' ),
( '3', '20518454', 'SD NEGERI 1 GONDOWANGI', 'JL. RAYA GONDOWANGI NO. 125', '009', '002', 'GONDOWANGI', 'WAGIR', 'MALANG', '65158', '-', 'sdngondowangi1@gmail.com', '-', 'SUKARJI ADI LEGOWO', '196904061997071001', '2024-01-06 09:04:55', '2024-01-06 09:04:55' ),
( '4', '20518384', 'SD NEGERI 1 JEDONG', 'JL. RAYA JEDONG NO. 146', '004', '001', 'JEDONG', 'WAGIR', 'MALANG', '65158', '-', 'sdn1jedong@gmail.com', '-', 'TUTIK JUWARIYAH', '-', '2024-01-06 09:04:55', '2024-01-06 09:04:55' ),
( '5', '20517168', 'SD NEGERI 1 MENDALANWANGI', 'JL. RAYA MENDALANWANGI 85, SEKARPUTIH', '015', '005', 'MENDALANWANGI', 'WAGIR', 'MALANG', '65158', '0341805698', 'sdnmendalanwangi01.wagir@gmail.com', 'http://sdnmendalanwangi01.sch.id', 'YUNI ASTUTI', '-', '2024-01-06 09:04:55', '2024-01-06 09:04:55' ),
( '6', '20517114', 'SD NEGERI 1 PANDANLANDUNG', 'JL. KALIJOGO NO 55', '006', '002', 'PANDANLANDUNG', 'WAGIR', 'MALANG', '65158', '0341536966', 'sdnpandansatu@gmail.com', '-', 'SRI MUDAYATI', '-', '2024-01-06 09:04:55', '2024-01-06 09:04:55' ),
( '7', '20517118', 'SD NEGERI 1 PANDANREJO', 'JL. RAYA PANDANSARI NO 75', '001', '001', 'PANDANREJO', 'WAGIR', 'MALANG', '65158', '-', 'sdnpandanrejo01_wagir@yahoo.co.id', '-', 'SUSI ERNAWATI', '-', '2024-01-06 09:04:55', '2024-01-06 09:04:55' ),
( '8', '20517295', 'SD NEGERI 1 PARANGARGO', 'JL. RAYA PARANGARGO GG. FLAMBOYAN NO 59', '009', '002', 'PARANGARGO', 'WAGIR', 'MALANG', '65158', '-', 'sdnparangargosatu@yahoo.co.id', '-', 'GATOT SUPARTO', '-', '2024-01-06 09:04:55', '2024-01-06 09:04:55' ),
( '9', '20517277', 'SD NEGERI 1 PETUNGSEWU', 'JL. RAYA PETUNGSEWU NO. 58', '014', '004', 'PETUNGSEWU', 'WAGIR', 'MALANG', '65158', '-', 'petungsewu1@gmail.com', '-', 'ROHMAN', '-', '2024-01-06 09:04:55', '2024-01-06 09:04:55' ),
( '10', '20516949', 'SD NEGERI 1 SIDORAHAYU', 'JL. RAYA SIDORAHAYU', '003', '001', 'SIDORAHAYU', 'WAGIR', 'MALANG', '65158', '-', 'sdrh01@yahoo.co.id', '-', 'KUMAYATI', '-', '2024-01-06 09:04:55', '2024-01-06 09:04:55' ),
( '11', '20516858', 'SD NEGERI 1 SITIREJO', 'DUSUN BUWEK', '002', '004', 'SITIREJO', 'WAGIR', 'MALANG', '65158', '-', 'sdnsitirejodua@yahoo.co.id', '-', 'SUNARDI', '-', '2024-01-06 09:04:55', '2024-01-06 09:04:55' ),
( '12', '20516859', 'SD NEGERI 1 SUKODADI', 'JL. RAYA GENDERAN', '016', '005', 'SUKODADI', 'WAGIR', 'MALANG', '65158', '-', 'sdnsukodadi01@gmail.com', '-', 'PUDJI SUSANTO', '-', '2024-01-06 09:04:55', '2024-01-06 09:04:55' ),
( '13', '69772577', 'SD NEGERI 1 SUMBERSUKO', 'JL. RAYA KENONGO NO 01', '008', '002', 'SUMBERSUKO', 'WAGIR', 'MALANG', '65158', '-', 'sdnegerisumbersuko1@gmail.com', '-', 'BUDI WIYONO', '-', '2024-01-06 09:04:55', '2024-01-06 09:04:55' ),
( '14', '20518471', 'SD NEGERI 2 GONDOWANGI', 'DUSUN WILOSO', '022', '004', 'GONDOWANGI', 'WAGIR', 'MALANG', '65158', '-', 'sdn_gondowangi02wagirmalang@yahoo.co.id', '-', 'DWI HARTATIK', '-', '2024-01-06 09:04:55', '2024-01-06 09:04:55' ),
( '15', '20518385', 'SD NEGERI 2 JEDONG', 'JL. RAYA JEDONG NO 270', '003', '009', 'JEDONG', 'WAGIR', 'MALANG', '65158', '-', 'sdnjedong02@yahoo.co.id', '-', 'DIANA LOITHA', '-', '2024-01-06 09:04:55', '2024-01-06 09:04:55' ),
( '16', '20517169', 'SD NEGERI 2 MENDALANWANGI', 'DUSUN MENDALAN WETAN', '018', '006', 'MENDALANWANGI', 'WAGIR', 'MALANG', '65158', '-', 'sdnmendalanwangi02@yahoo.com', '-', 'SUPARDI', '-', '2024-01-06 09:04:55', '2024-01-06 09:04:55' ),
( '17', '20517115', 'SD NEGERI 2 PANDANLANDUNG', 'JL. GAPURA NO 121', '018', '004', 'PANDANLANDUNG', 'WAGIR', 'MALANG', '65158', '0341589200', 'sdnpandanlandung02@yahoo.com', 'http://sdnpandanlandung02.com', 'GATOT SUPARTO', '-', '2024-01-06 09:04:55', '2024-01-06 09:04:55' ),
( '18', '20517121', 'SD NEGERI 2 PANDANREJO', 'JL. RAYA NGRAGI', '017', '006', 'PANDANREJO', 'WAGIR', 'MALANG', '65158', '-', 'sdn2pandanrejowgr@gmail.com', '-', 'BUDIONO', '-', '2024-01-06 09:04:55', '2024-01-06 09:04:55' ),
( '19', '20518723', 'SD NEGERI 2 PARANGARGO', 'JL. JUWETMANTING NO 2', '014', '004', 'PARANGARGO', 'WAGIR', 'MALANG', '65158', '6281315372374', 'sdnparangargo2@gmail.com', '-', 'SITI FATIMAH', '-', '2024-01-06 09:04:55', '2024-01-06 09:04:55' ),
( '20', '20517278', 'SD NEGERI 2 PETUNGSEWU', 'JL. RAYA CODO', '004', '002', 'PETUNGSEWU', 'WAGIR', 'MALANG', '65158', '-', 'sdnduapetungsewu@gmail.com', '-', 'IDA YUNIARSI', '-', '2024-01-06 09:04:55', '2024-01-06 09:04:55' ),
( '21', '20516950', 'SD NEGERI 2 SIDORAHAYU', 'JL. GATOTKACA NO 32', '015', '003', 'SIDORAHAYU', 'WAGIR', 'MALANG', '65158', '-', 'sidorahayu02wagir@yahoo.co.id', '-', 'KIYANTO', '-', '2024-01-06 09:04:55', '2024-01-06 09:04:55' ),
( '22', '20516845', 'SD NEGERI 2 SITIREJO', 'DUSUN MBUWEK', '002', '004', 'SITIREJO', 'WAGIR', 'MALANG', '65158', '-', 'sdnsitirejodua@yahoo.co.id', '-', 'SUNARDI', '-', '2024-01-06 09:04:55', '2024-01-06 09:04:55' ),
( '23', '20516860', 'SD NEGERI 2 SUKODADI', 'JL. RAYA JAMURAN NO 46', '002', '001', 'SUKODADI', 'WAGIR', 'MALANG', '65158', '-', 'sdn_sukodadi02@yahoo.co.id', '-', 'ISTIANAH', '-', '2024-01-06 09:04:55', '2024-01-06 09:04:55' ),
( '24', '20516971', 'SD NEGERI 2 SUMBERSUKO', 'DUSUN SUMBERPANG LOR', '016', '004', 'SUMBERSUKO', 'WAGIR', 'MALANG', '65158', '-', 'sdnsumbersuko02@ymail.com', '-', 'YUNI ASTUTI', '-', '2024-01-06 09:04:55', '2024-01-06 09:04:55' ),
( '25', '20518850', 'SD NEGERI 3 BEDALISODO', 'JL. COBAN GLOTAK', '031', '010', 'DALISODO', 'WAGIR', 'MALANG', '65158', '-', 'sdn_bedali03@yahoo.com', '-', 'HASAN LUTFI', '-', '2024-01-06 09:04:55', '2024-01-06 09:04:55' ),
( '26', '20518472', 'SD NEGERI 3 GONDOWANGI', 'JL. DAWUHAN NO 9', '014', '003', 'GONDOWANGI', 'WAGIR', 'MALANG', '65158', '0341525676', 'sdngondowangi3@gmail.com', '-', 'YUNI ASTUTI', '-', '2024-01-06 09:04:55', '2024-01-06 09:04:55' ),
( '27', '20518386', 'SD NEGERI 3 JEDONG', 'DUKUH JATEN', '002', '004', 'JEDONG', 'WAGIR', 'MALANG', '65158', '-', 'sdnjedong03_wagir@yahoo.co.id', '-', 'ATIK WIDARYATI', '-', '2024-01-06 09:04:55', '2024-01-06 09:04:55' ),
( '28', '20517170', 'SD NEGERI 3 MENDALANWANGI', 'DUSUN DARUNGAN', '025', '009', 'MENDALANWANGI', 'WAGIR', 'MALANG', '65158', '-', 'sdnmendalanwangi03wagirmalang@gmail.com', '-', 'MUJIONO', '-', '2024-01-06 09:04:55', '2024-01-06 09:04:55' ),
( '29', '20517142', 'SD NEGERI 3 PANDANLANDUNG', 'JL. GUNUNGJATI NO 210', '020', '005', 'PANDANLANDUNG', 'WAGIR', 'MALANG', '65158', '-', 'pandanlandungtiga@gmail.com', '-', 'ABDUL GHAFUR', '-', '2024-01-06 09:04:55', '2024-01-06 09:04:55' ),
( '30', '20516951', 'SD NEGERI 3 SIDORAHAYU', 'JL. RAYA SIDORAHAYU', '003', '001', 'SIDORAHAYU', 'WAGIR', 'MALANG', '65158', '-', 'sdn3sidorahayu@gmail.com', '-', 'WIJIATUN', '-', '2024-01-06 09:04:55', '2024-01-06 09:04:55' ),
( '31', '20516846', 'SD NEGERI 3 SITIREJO', 'DUSUN TEMU', '001', '003', 'SITIREJO', 'WAGIR', 'MALANG', '65158', '-', 'sdnsitirejotiga@gmail.com', '-', 'PUJIONO', '-', '2024-01-06 09:04:55', '2024-01-06 09:04:55' ),
( '32', '20518851', 'SD NEGERI 4 BEDALISODO', 'DUSUN WANGKAL', '035', '011', 'DALISODO', 'WAGIR', 'MALANG', '65158', '-', 'sdnbedalisodo04_wagir@yahoo.co.id', '-', 'SLAMET HARIADI', '-', '2024-01-06 09:04:55', '2024-01-06 09:04:55' ),
( '33', '20516847', 'SD NEGERI 4 SITIREJO', 'JL. RAYA SITIREJO 110', '008', '001', 'SITIREJO', 'WAGIR', 'MALANG', '65158', '-', 'sdsitirejoempat@gmail.com', '-', 'WIJI UTAMI', '-', '2024-01-06 09:04:55', '2024-01-06 09:04:55' ),
( '34', '20516973', 'SD NEGERI 4 SUMBERSUKO', 'JL. RAYA PRECET NO. 6', '025', '008', 'SUMBERSUKO', 'WAGIR', 'MALANG', '65158', '-', 'sdnsumbersuko04@yahoo.com', '-', 'JAYADI', '-', '2024-01-06 09:04:55', '2024-01-06 09:04:55' );
COMMIT;
-- ---------------------------------------------------------


-- Dump data of "siswas" -----------------------------------
BEGIN;

INSERT INTO `siswas`(`id`,`nisn`,`nama`,`jk`,`sekolah_id`,`rombel_id`,`created_at`,`updated_at`) VALUES 
( '22', '3155065202', 'YUFAN PRADITYA KURNIAWAN', 'Laki-laki', '20518848', '23242-20518848-1', '2024-01-06 09:56:01', '2024-01-06 09:56:01' ),
( '23', '3164899126', 'AHMAD BAGUS PRATAMA', 'Laki-laki', '20518848', '23242-20518848-1', '2024-01-06 09:56:01', '2024-01-06 09:56:01' ),
( '24', '3170903942', 'AHMAD FATAR NUR CAHYO', 'Laki-laki', '20518848', '23242-20518848-1', '2024-01-06 09:56:01', '2024-01-06 09:56:01' ),
( '25', '3169753560', 'AHMAD NUR RIDWAN', 'Laki-laki', '20518848', '23242-20518848-1', '2024-01-06 09:56:01', '2024-01-06 09:56:01' ),
( '26', '3164256249', 'AISYAH HASNA SHOFIYAH', 'Perempuan', '20518848', '23242-20518848-1', '2024-01-06 09:56:01', '2024-01-06 09:56:01' ),
( '27', '3169466678', 'AKBAR FATUROHMAN SURYA WINATA', 'Laki-laki', '20518848', '23242-20518848-1', '2024-01-06 09:56:01', '2024-01-06 09:56:01' ),
( '28', '3169445845', 'AWAIS JULFIANA', 'Perempuan', '20518848', '23242-20518848-1', '2024-01-06 09:56:01', '2024-01-06 09:56:01' ),
( '29', '3176180628', 'AZIO ATALA AZIZI', 'Laki-laki', '20518848', '23242-20518848-1', '2024-01-06 09:56:01', '2024-01-06 09:56:01' ),
( '30', '3175671335', 'AZKIA AZAHRA', 'Perempuan', '20518848', '23242-20518848-1', '2024-01-06 09:56:01', '2024-01-06 09:56:01' ),
( '31', '3161876368', 'DHEA PUTRI SALSABILA', 'Perempuan', '20518848', '23242-20518848-1', '2024-01-06 09:56:01', '2024-01-06 09:56:01' ),
( '32', '3169991572', 'ELVAN FADLI GAVIN DWI ANDRIANNTA', 'Laki-laki', '20518848', '23242-20518848-1', '2024-01-06 09:56:01', '2024-01-06 09:56:01' ),
( '33', '3176012330', 'ELVIANA ALISYA', 'Perempuan', '20518848', '23242-20518848-1', '2024-01-06 09:56:01', '2024-01-06 09:56:01' ),
( '34', '3169564805', 'ENJELITA AZZAHRA BERLIANTI', 'Perempuan', '20518848', '23242-20518848-1', '2024-01-06 09:56:01', '2024-01-06 09:56:01' ),
( '35', '3162818270', 'GALIH ALVARO ARZIKI', 'Laki-laki', '20518848', '23242-20518848-1', '2024-01-06 09:56:01', '2024-01-06 09:56:01' ),
( '36', '3162613554', 'GILSA NUR FADILAH', 'Perempuan', '20518848', '23242-20518848-1', '2024-01-06 09:56:01', '2024-01-06 09:56:01' ),
( '37', '3169353932', 'HANIFATUL PUTRI MAIDHIROH', 'Perempuan', '20518848', '23242-20518848-1', '2024-01-06 09:56:01', '2024-01-06 09:56:01' ),
( '38', '3164373282', 'HILDA ITSNA SUWIKNYO', 'Perempuan', '20518848', '23242-20518848-1', '2024-01-06 09:56:01', '2024-01-06 09:56:01' ),
( '39', '3179996040', 'INANI AISYAH LAILATUL SYADIYAH', 'Perempuan', '20518848', '23242-20518848-1', '2024-01-06 09:56:01', '2024-01-06 09:56:01' ),
( '40', '3164125075', 'JONI PRASETYO', 'Laki-laki', '20518848', '23242-20518848-1', '2024-01-06 09:56:01', '2024-01-06 09:56:01' ),
( '41', '3165821103', 'KIRANIA AZZAHRA MAGFIRAH', 'Perempuan', '20518848', '23242-20518848-1', '2024-01-06 09:56:01', '2024-01-06 09:56:01' ),
( '42', '3162775021', 'MAYHEL JORDAN RAMADANI', 'Laki-laki', '20518848', '23242-20518848-1', '2024-01-06 09:56:01', '2024-01-06 09:56:01' ),
( '43', '0987654321', 'MOCH. RAFFASYA NANDA KARIONO', 'Laki-laki', '20518848', '23242-20518848-1', '2024-01-06 09:56:01', '2024-01-06 09:56:01' ),
( '44', '3162163251', 'MUHAMAD JULIANZA', 'Laki-laki', '20518848', '23242-20518848-1', '2024-01-06 09:56:01', '2024-01-06 09:56:01' ),
( '45', '3170544080', 'MUHAMAD RAFISQY HAIKAL SANAULLAH', 'Laki-laki', '20518848', '23242-20518848-1', '2024-01-06 09:56:01', '2024-01-06 09:56:01' ),
( '46', '3165249170', 'MUHAMMAD ADAM SYAFIQ MAULANA', 'Laki-laki', '20518848', '23242-20518848-1', '2024-01-06 09:56:01', '2024-01-06 09:56:01' ),
( '47', '3168497889', 'MUHAMMAD AHDA ARSYAD RAFANDA', 'Laki-laki', '20518848', '23242-20518848-1', '2024-01-06 09:56:01', '2024-01-06 09:56:01' ),
( '48', '3164439157', 'MUHAMMAD NUR HUDA', 'Laki-laki', '20518848', '23242-20518848-1', '2024-01-06 09:56:01', '2024-01-06 09:56:01' ),
( '49', '3161263488', 'MUHAMMAD RADITYA PRATAMA', 'Laki-laki', '20518848', '23242-20518848-1', '2024-01-06 09:56:01', '2024-01-06 09:56:01' ),
( '50', '3166203704', 'MUHAMMAD RIKALDO SAPUTRA', 'Laki-laki', '20518848', '23242-20518848-1', '2024-01-06 09:56:01', '2024-01-06 09:56:01' ),
( '51', '3170218882', 'MUHAMMAD TEGAR PRAYOGO', 'Laki-laki', '20518848', '23242-20518848-1', '2024-01-06 09:56:01', '2024-01-06 09:56:01' ),
( '52', '3162250742', 'PUTRI MEINANDA CAHYANI SALSABILA', 'Perempuan', '20518848', '23242-20518848-1', '2024-01-06 09:56:01', '2024-01-06 09:56:01' ),
( '53', '0161701781', 'RAFI PUTRA AL-FARONI', 'Laki-laki', '20518848', '23242-20518848-1', '2024-01-06 09:56:01', '2024-01-06 09:56:01' ),
( '54', '3168285261', 'RAFIFA AMELIA AS ZHAHRO', 'Perempuan', '20518848', '23242-20518848-1', '2024-01-06 09:56:01', '2024-01-06 09:56:01' ),
( '55', '3163966080', 'RAKHA ABHINAYA ARDIAN RAMADHANI', 'Laki-laki', '20518848', '23242-20518848-1', '2024-01-06 09:56:01', '2024-01-06 09:56:01' ),
( '56', '3162332005', 'SELENA SALSABILA', 'Perempuan', '20518848', '23242-20518848-1', '2024-01-06 09:56:01', '2024-01-06 09:56:01' ),
( '57', '3165905604', 'WINDAYU PERMATA SARI', 'Perempuan', '20518848', '23242-20518848-1', '2024-01-06 09:56:01', '2024-01-06 09:56:01' ),
( '94', '0136930632', 'ADELIA FARAH NISA ZEIN', 'Perempuan', '20518848', '23242-20518848-2', '2024-01-06 10:00:13', '2024-01-06 10:00:13' ),
( '95', '3154327958', 'ALBI HARDIKA ALDIANO', 'Laki-laki', '20518848', '23242-20518848-2', '2024-01-06 10:00:13', '2024-01-06 10:00:13' ),
( '96', '3151170568', 'ALESSANDRA ANJELITA SYAKIRA', 'Perempuan', '20518848', '23242-20518848-2', '2024-01-06 10:00:13', '2024-01-06 10:00:13' ),
( '97', '3154351330', 'ANINDITA NOUVA KUSUMAWARDHANI', 'Perempuan', '20518848', '23242-20518848-2', '2024-01-06 10:00:13', '2024-01-06 10:00:13' ),
( '98', '3159746487', 'ARJUN ADI PUTRA PRATAMA', 'Laki-laki', '20518848', '23242-20518848-2', '2024-01-06 10:00:13', '2024-01-06 10:00:13' ),
( '99', '3151377443', 'ARYA PUTRA ALFIYANTO', 'Laki-laki', '20518848', '23242-20518848-2', '2024-01-06 10:00:13', '2024-01-06 10:00:13' ),
( '100', '0138483489', 'ASKIA MAR\'ATUSSOLIHAH', 'Perempuan', '20518848', '23242-20518848-2', '2024-01-06 10:00:13', '2024-01-06 10:00:13' ),
( '101', '0139969721', 'AZZAHRA DZIKRINA DITA VALENTINA IRAWANTO', 'Perempuan', '20518848', '23242-20518848-2', '2024-01-06 10:00:13', '2024-01-06 10:00:13' ),
( '102', '3159660713', 'BAKDIATUS SHOLEH', 'Perempuan', '20518848', '23242-20518848-2', '2024-01-06 10:00:13', '2024-01-06 10:00:13' ),
( '103', '3152590752', 'DIAZ RAKA BRILIANSYAH', 'Laki-laki', '20518848', '23242-20518848-2', '2024-01-06 10:00:13', '2024-01-06 10:00:13' ),
( '104', '3153629955', 'FATIMAH LAILATUL ZAHRA', 'Perempuan', '20518848', '23242-20518848-2', '2024-01-06 10:00:13', '2024-01-06 10:00:13' ),
( '105', '3153795071', 'HILMI BHIBBAH RAMADHANI', 'Laki-laki', '20518848', '23242-20518848-2', '2024-01-06 10:00:13', '2024-01-06 10:00:13' ),
( '106', '3162658200', 'INES KARTIKA PUTRI', 'Perempuan', '20518848', '23242-20518848-2', '2024-01-06 10:00:13', '2024-01-06 10:00:13' ),
( '107', '3156638920', 'KENZIO PUTRA PRATAMA', 'Laki-laki', '20518848', '23242-20518848-2', '2024-01-06 10:00:13', '2024-01-06 10:00:13' ),
( '108', '3163220620', 'M. ABIYAN ALVIS ALKHALIFI', 'Laki-laki', '20518848', '23242-20518848-2', '2024-01-06 10:00:13', '2024-01-06 10:00:13' ),
( '109', '3156320347', 'MUHAMAD HAFIZ IRWAN SEPTIAN', 'Laki-laki', '20518848', '23242-20518848-2', '2024-01-06 10:00:13', '2024-01-06 10:00:13' ),
( '110', '3151490799', 'MUHAMMAD SANDI YUSUF ANTON ALMAULANA', 'Laki-laki', '20518848', '23242-20518848-2', '2024-01-06 10:00:13', '2024-01-06 10:00:13' ),
( '111', '3151805605', 'NAFISAH AYUNDA ZAHRA', 'Perempuan', '20518848', '23242-20518848-2', '2024-01-06 10:00:13', '2024-01-06 10:00:13' ),
( '112', '3150300076', 'NASILA NUR EVITA SARI', 'Perempuan', '20518848', '23242-20518848-2', '2024-01-06 10:00:13', '2024-01-06 10:00:13' ),
( '113', '3153037053', 'NIA RAHMAWATI', 'Perempuan', '20518848', '23242-20518848-2', '2024-01-06 10:00:13', '2024-01-06 10:00:13' ),
( '114', '3152390955', 'REVA PUTRI OCTAVIANI', 'Perempuan', '20518848', '23242-20518848-2', '2024-01-06 10:00:13', '2024-01-06 10:00:13' ),
( '115', '3155599209', 'RISKY RAMADIANSYAH', 'Laki-laki', '20518848', '23242-20518848-2', '2024-01-06 10:00:13', '2024-01-06 10:00:13' ),
( '116', '3159904426', 'RIVA AINUR SAFIRA', 'Perempuan', '20518848', '23242-20518848-2', '2024-01-06 10:00:13', '2024-01-06 10:00:13' ),
( '117', '0132621287', 'RIZKHI SEPTIAN PUTRA', 'Laki-laki', '20518848', '23242-20518848-2', '2024-01-06 10:00:13', '2024-01-06 10:00:13' ),
( '118', '3150556448', 'ROBI AZKA BINTANG PAMUNGKAS', 'Laki-laki', '20518848', '23242-20518848-2', '2024-01-06 10:00:13', '2024-01-06 10:00:13' ),
( '119', '3156002629', 'YAFIQ FADHIL AZZAKY', 'Laki-laki', '20518848', '23242-20518848-2', '2024-01-06 10:00:13', '2024-01-06 10:00:13' ),
( '120', '3130131122', 'ANGGER RAMADAN', 'Laki-laki', '20518848', '23242-20518848-3', '2024-01-06 10:08:14', '2024-01-06 10:08:14' ),
( '121', '0138062525', 'REVAN ALIF PRASETYA', 'Laki-laki', '20518848', '23242-20518848-3', '2024-01-06 10:08:14', '2024-01-06 10:08:14' ),
( '122', '0141719343', 'ADZKIA SAMHA SAUFA DEFITA WARDANI', 'Perempuan', '20518848', '23242-20518848-3', '2024-01-06 10:08:14', '2024-01-06 10:08:14' ),
( '123', '3148282133', 'Alvio Septia Az-zahwa', 'Perempuan', '20518848', '23242-20518848-3', '2024-01-06 10:08:14', '2024-01-06 10:08:14' ),
( '124', '0143385302', 'Amanda Salsabila Putri', 'Perempuan', '20518848', '23242-20518848-3', '2024-01-06 10:08:14', '2024-01-06 10:08:14' ),
( '125', '3142533713', 'Aprilia Jihan Avivi', 'Perempuan', '20518848', '23242-20518848-3', '2024-01-06 10:08:14', '2024-01-06 10:08:14' ),
( '126', '3145311087', 'Arik Muhammad Novan', 'Laki-laki', '20518848', '23242-20518848-3', '2024-01-06 10:08:14', '2024-01-06 10:08:14' ),
( '127', '3154518482', 'Bilqis Azahra Savira', 'Perempuan', '20518848', '23242-20518848-3', '2024-01-06 10:08:14', '2024-01-06 10:08:14' ),
( '128', '3141781345', 'Chelsea Nur Assyifa Azzahra', 'Perempuan', '20518848', '23242-20518848-3', '2024-01-06 10:08:14', '2024-01-06 10:08:14' ),
( '129', '0132836937', 'DEVAN RIZAL ARYA PUTRA', 'Laki-laki', '20518848', '23242-20518848-3', '2024-01-06 10:08:14', '2024-01-06 10:08:14' ),
( '130', '3141317849', 'Dhallisya Permata Az-zahra', 'Perempuan', '20518848', '23242-20518848-3', '2024-01-06 10:08:14', '2024-01-06 10:08:14' ),
( '131', '0148649284', 'ELSA OKTAFIANIS', 'Perempuan', '20518848', '23242-20518848-3', '2024-01-06 10:08:14', '2024-01-06 10:08:14' ),
( '132', '3148023221', 'Fiko Saputra', 'Laki-laki', '20518848', '23242-20518848-3', '2024-01-06 10:08:14', '2024-01-06 10:08:14' ),
( '133', '3157154187', 'Hanifa Meladyva', 'Perempuan', '20518848', '23242-20518848-3', '2024-01-06 10:08:14', '2024-01-06 10:08:14' ),
( '134', '0142003579', 'KAYLA DWI RAHMADHANI', 'Perempuan', '20518848', '23242-20518848-3', '2024-01-06 10:08:14', '2024-01-06 10:08:14' ),
( '135', '3141393528', 'Muhammad Eka Aldiansyah', 'Laki-laki', '20518848', '23242-20518848-3', '2024-01-06 10:08:14', '2024-01-06 10:08:14' ),
( '136', '3142234213', 'Muhammat Aga Fitroni', 'Laki-laki', '20518848', '23242-20518848-3', '2024-01-06 10:08:14', '2024-01-06 10:08:14' ),
( '137', '0155020272', 'NGADI SANTOSO', 'Laki-laki', '20518848', '23242-20518848-3', '2024-01-06 10:08:14', '2024-01-06 10:08:14' ),
( '138', '0142412103', 'NIRINA AZAHRA RAHMADIANI', 'Perempuan', '20518848', '23242-20518848-3', '2024-01-06 10:08:14', '2024-01-06 10:08:14' ),
( '139', '0157365902', 'NURMA MAULITASARI', 'Perempuan', '20518848', '23242-20518848-3', '2024-01-06 10:08:14', '2024-01-06 10:08:14' ),
( '140', '0143364459', 'OKTAVIA SRI RAHAYU', 'Perempuan', '20518848', '23242-20518848-3', '2024-01-06 10:08:14', '2024-01-06 10:08:14' ),
( '141', '3155608833', 'Resti Maulidiya Rosita', 'Perempuan', '20518848', '23242-20518848-3', '2024-01-06 10:08:14', '2024-01-06 10:08:14' ),
( '142', '0141088132', 'SHAFIRA MAULIDYAH FIRDAUS', 'Perempuan', '20518848', '23242-20518848-3', '2024-01-06 10:08:14', '2024-01-06 10:08:14' ),
( '143', '0155216969', 'ILHAM DIRGA HARDIANSYAH', 'Laki-laki', '20518848', '23242-20518848-3', '2024-01-06 10:08:14', '2024-01-06 10:08:14' ),
( '144', '0148801640', 'MUHAMMAD FEBRI FIRMANSYAH', 'Laki-laki', '20518848', '23242-20518848-3', '2024-01-06 10:08:14', '2024-01-06 10:08:14' ),
( '145', '3148545026', 'Vareyhan Al Zano', 'Laki-laki', '20518848', '23242-20518848-3', '2024-01-06 10:08:14', '2024-01-06 10:08:14' ),
( '215', '3136101473', 'ADITIYA AINNUROCHMAN', 'Laki-laki', '20518848', '23242-20518848-4', '2024-01-06 10:14:24', '2024-01-06 10:14:24' ),
( '216', '0147427338', 'ALBI LUDFI PRANAJA', 'Laki-laki', '20518848', '23242-20518848-4', '2024-01-06 10:14:24', '2024-01-06 10:14:24' ),
( '217', '3133162603', 'ANGGA HAVID SETYAWAN', 'Laki-laki', '20518848', '23242-20518848-4', '2024-01-06 10:14:24', '2024-01-06 10:14:24' ),
( '218', '3138428643', 'AULIA SAVIRA PUTRI', 'Perempuan', '20518848', '23242-20518848-4', '2024-01-06 10:14:24', '2024-01-06 10:14:24' ),
( '219', '3132344891', 'BAGAS ADI PUTRA PRATAMA', 'Laki-laki', '20518848', '23242-20518848-4', '2024-01-06 10:14:24', '2024-01-06 10:14:24' ),
( '220', '0139241483', 'DIMAS BAGUS SAJIWO', 'Laki-laki', '20518848', '23242-20518848-4', '2024-01-06 10:14:24', '2024-01-06 10:14:24' ),
( '221', '3137260700', 'FATIMATUZ ZAHRA', 'Perempuan', '20518848', '23242-20518848-4', '2024-01-06 10:14:24', '2024-01-06 10:14:24' ),
( '222', '0131178326', 'HANA KHALIZA', 'Perempuan', '20518848', '23242-20518848-4', '2024-01-06 10:14:24', '2024-01-06 10:14:24' ),
( '223', '0134372897', 'JIHAN OKTAVIA ANJANI', 'Perempuan', '20518848', '23242-20518848-4', '2024-01-06 10:14:24', '2024-01-06 10:14:24' ),
( '224', '0133715403', 'NADZIRA DZAKIYATUL ILMA', 'Perempuan', '20518848', '23242-20518848-4', '2024-01-06 10:14:24', '2024-01-06 10:14:24' ),
( '225', '0136629317', 'NANDA KHOIROTUL FAUZIAH', 'Perempuan', '20518848', '23242-20518848-4', '2024-01-06 10:14:24', '2024-01-06 10:14:24' ),
( '226', '0143192715', 'NAUREL AULIA FEBRIANTI', 'Perempuan', '20518848', '23242-20518848-4', '2024-01-06 10:14:24', '2024-01-06 10:14:24' ),
( '227', '0146546021', 'RADITYA NAUFAL FIRMANSYAH', 'Laki-laki', '20518848', '23242-20518848-4', '2024-01-06 10:14:24', '2024-01-06 10:14:24' ),
( '228', '0132764273', 'REDO SYAFRIL HERMAWAN', 'Laki-laki', '20518848', '23242-20518848-4', '2024-01-06 10:14:24', '2024-01-06 10:14:24' ),
( '229', '3134399507', 'SELLA AMANDA CALLISTA', 'Perempuan', '20518848', '23242-20518848-4', '2024-01-06 10:14:24', '2024-01-06 10:14:24' ),
( '230', '0131954956', 'SERLLY NOVIANTI AULIA', 'Perempuan', '20518848', '23242-20518848-4', '2024-01-06 10:14:24', '2024-01-06 10:14:24' ),
( '231', '3143887681', 'WASIFAH', 'Perempuan', '20518848', '23242-20518848-4', '2024-01-06 10:14:24', '2024-01-06 10:14:24' ),
( '232', '0145664682', 'ZAHWA ALMA NADHIFA MAULIDA', 'Perempuan', '20518848', '23242-20518848-4', '2024-01-06 10:14:24', '2024-01-06 10:14:24' ),
( '233', '3130106603', 'ZASKIA NAYLA PUTRI MAULIDYA', 'Perempuan', '20518848', '23242-20518848-4', '2024-01-06 10:14:24', '2024-01-06 10:14:24' ),
( '234', '0134339237', 'ZULFA MAHDIYAH', 'Perempuan', '20518848', '23242-20518848-4', '2024-01-06 10:14:24', '2024-01-06 10:14:24' ),
( '235', '3137437197', 'AFRILIA TRIA AZ-ZAHRA', 'Perempuan', '20518848', '23242-20518848-5', '2024-01-06 10:14:46', '2024-01-06 10:14:46' ),
( '236', '0119149739', 'ALIF AINUR RIZA RAMADHANI', 'Perempuan', '20518848', '23242-20518848-5', '2024-01-06 10:14:46', '2024-01-06 10:14:46' ),
( '237', '3125164436', 'ANDRA RAMADHAN', 'Laki-laki', '20518848', '23242-20518848-5', '2024-01-06 10:14:46', '2024-01-06 10:14:46' ),
( '238', '3126478492', 'ARDAN FIRLY BHIAN PRIBADI', 'Laki-laki', '20518848', '23242-20518848-5', '2024-01-06 10:14:46', '2024-01-06 10:14:46' ),
( '239', '3122381846', 'AZCA TRIO OCTAFIYANO', 'Laki-laki', '20518848', '23242-20518848-5', '2024-01-06 10:14:46', '2024-01-06 10:14:46' ),
( '240', '3112504785', 'BINAR BINTANG KEJORA', 'Perempuan', '20518848', '23242-20518848-5', '2024-01-06 10:14:46', '2024-01-06 10:14:46' ),
( '241', '3139411186', 'CHELSEA KEIZA ADHELLA', 'Perempuan', '20518848', '23242-20518848-5', '2024-01-06 10:14:46', '2024-01-06 10:14:46' ),
( '242', '3118361236', 'ERIKA RISMA TRIWIJAYANTI', 'Perempuan', '20518848', '23242-20518848-5', '2024-01-06 10:14:46', '2024-01-06 10:14:46' ),
( '243', '3122078673', 'HAFIZD IKHSAN FADIL', 'Laki-laki', '20518848', '23242-20518848-5', '2024-01-06 10:14:46', '2024-01-06 10:14:46' ),
( '244', '0124352213', 'JOKO SUSILO', 'Laki-laki', '20518848', '23242-20518848-5', '2024-01-06 10:14:46', '2024-01-06 10:14:46' ),
( '245', '3125956138', 'MUHAMAD HAFIDZ SAPUTRA', 'Laki-laki', '20518848', '23242-20518848-5', '2024-01-06 10:14:46', '2024-01-06 10:14:46' ),
( '246', '3121918396', 'MUHAMMAD FAREL FEBBYANSYA', 'Laki-laki', '20518848', '23242-20518848-5', '2024-01-06 10:14:46', '2024-01-06 10:14:46' ),
( '247', '0113259495', 'MUHAMMAD RISQI', 'Laki-laki', '20518848', '23242-20518848-5', '2024-01-06 10:14:46', '2024-01-06 10:14:46' ),
( '248', '0114742148', 'PUTRA AMIN MUHAMAD HAMSYAH', 'Laki-laki', '20518848', '23242-20518848-5', '2024-01-06 10:14:46', '2024-01-06 10:14:46' ),
( '249', '3123939573', 'PUTRA RAMA ADITYA PRATAMA', 'Laki-laki', '20518848', '23242-20518848-5', '2024-01-06 10:14:46', '2024-01-06 10:14:46' ),
( '250', '3121599932', 'RAVA SATRIA RAMADHANI', 'Laki-laki', '20518848', '23242-20518848-5', '2024-01-06 10:14:46', '2024-01-06 10:14:46' ),
( '251', '0136748941', 'RISA NUR PITA', 'Perempuan', '20518848', '23242-20518848-5', '2024-01-06 10:14:46', '2024-01-06 10:14:46' ),
( '252', '3125312138', 'RISKA FATMA AZZAHRA', 'Perempuan', '20518848', '23242-20518848-5', '2024-01-06 10:14:46', '2024-01-06 10:14:46' ),
( '253', '3137976166', 'ROBITA DIANDRA SETYANINGRUM', 'Perempuan', '20518848', '23242-20518848-5', '2024-01-06 10:14:46', '2024-01-06 10:14:46' ),
( '254', '0128448884', 'TIARI NUR DEFANTI', 'Perempuan', '20518848', '23242-20518848-5', '2024-01-06 10:14:46', '2024-01-06 10:14:46' ),
( '255', '0133881371', 'SILVIA FATMA YUDHA VEBRIANA', 'Perempuan', '20518848', '23242-20518848-5', '2024-01-06 10:14:46', '2024-01-06 10:14:46' ),
( '256', '3129860871', 'RISKA ADELIA YASMIN', 'Perempuan', '20518848', '23242-20518848-5', '2024-01-06 10:14:46', '2024-01-06 10:14:46' ),
( '257', '3119694490', 'VIVI NUR AINI', 'Perempuan', '20518848', '23242-20518848-5', '2024-01-06 10:14:46', '2024-01-06 10:14:46' ),
( '258', '0126114915', 'ABDUL HAFIZH IBRAHIM', 'Laki-laki', '20518848', '23242-20518848-6', '2024-01-06 10:15:05', '2024-01-06 10:15:05' ),
( '259', '0116958836', 'ADELIA RAMADANI', 'Perempuan', '20518848', '23242-20518848-6', '2024-01-06 10:15:05', '2024-01-06 10:15:05' ),
( '260', '0113579769', 'AHMAD ARIF FADIL', 'Laki-laki', '20518848', '23242-20518848-6', '2024-01-06 10:15:05', '2024-01-06 10:15:05' ),
( '261', '0109481336', 'AHMAD FAJAR IRAWAN', 'Laki-laki', '20518848', '23242-20518848-6', '2024-01-06 10:15:05', '2024-01-06 10:15:05' ),
( '262', '0106308754', 'AHMAD FAREL ARDIANSYAH', 'Laki-laki', '20518848', '23242-20518848-6', '2024-01-06 10:15:05', '2024-01-06 10:15:05' ),
( '263', '0114167149', 'AISYA SALSABILA', 'Perempuan', '20518848', '23242-20518848-6', '2024-01-06 10:15:05', '2024-01-06 10:15:05' ),
( '264', '0113163642', 'ALDY FERDIANSYAH', 'Laki-laki', '20518848', '23242-20518848-6', '2024-01-06 10:15:05', '2024-01-06 10:15:05' ),
( '265', '0115636911', 'Anida Musyfirotul Isnaini', 'Perempuan', '20518848', '23242-20518848-6', '2024-01-06 10:15:05', '2024-01-06 10:15:05' ),
( '266', '0114051057', 'AULIA QULYU NURJANAH', 'Perempuan', '20518848', '23242-20518848-6', '2024-01-06 10:15:05', '2024-01-06 10:15:05' ),
( '267', '0107359082', 'BELLA YULIA RAHMA', 'Perempuan', '20518848', '23242-20518848-6', '2024-01-06 10:15:05', '2024-01-06 10:15:05' ),
( '268', '0124619058', 'Belqis Putri Anggraeni', 'Perempuan', '20518848', '23242-20518848-6', '2024-01-06 10:15:05', '2024-01-06 10:15:05' ),
( '269', '0111762281', 'DICTA AHMAD FERDIANSYA', 'Laki-laki', '20518848', '23242-20518848-6', '2024-01-06 10:15:05', '2024-01-06 10:15:05' ),
( '270', '0112969366', 'Echa Dwi Oktora', 'Perempuan', '20518848', '23242-20518848-6', '2024-01-06 10:15:05', '2024-01-06 10:15:05' ),
( '271', '0117275700', 'FINDHA SASKIA REVALINA', 'Perempuan', '20518848', '23242-20518848-6', '2024-01-06 10:15:05', '2024-01-06 10:15:05' ),
( '272', '0117428125', 'IKROM ACHMAD JUNAIDI', 'Laki-laki', '20518848', '23242-20518848-6', '2024-01-06 10:15:05', '2024-01-06 10:15:05' ),
( '273', '0116107363', 'INTAN SARI', 'Perempuan', '20518848', '23242-20518848-6', '2024-01-06 10:15:05', '2024-01-06 10:15:05' ),
( '274', '0117194139', 'JULIANDRA ADIPRATAMA', 'Laki-laki', '20518848', '23242-20518848-6', '2024-01-06 10:15:05', '2024-01-06 10:15:05' ),
( '275', '0119718765', 'KHOLIFATUL FITRIA NANDA KARIN', 'Perempuan', '20518848', '23242-20518848-6', '2024-01-06 10:15:05', '2024-01-06 10:15:05' ),
( '276', '0113444117', 'LIVYA ZHIFA RAMADHANI', 'Perempuan', '20518848', '23242-20518848-6', '2024-01-06 10:15:05', '2024-01-06 10:15:05' ),
( '277', '0118725945', 'MAYA ZAHRA PUTRI SALSABILLA', 'Perempuan', '20518848', '23242-20518848-6', '2024-01-06 10:15:05', '2024-01-06 10:15:05' ),
( '278', '0102175284', 'MIHAN AULIA AFANDI', 'Laki-laki', '20518848', '23242-20518848-6', '2024-01-06 10:15:05', '2024-01-06 10:15:05' ),
( '279', '0126455747', 'MISKA SITI NUR AISYAH', 'Perempuan', '20518848', '23242-20518848-6', '2024-01-06 10:15:05', '2024-01-06 10:15:05' ),
( '280', '0111079626', 'MOCHAMAD SYAFA\' MISBACHUN NUR', 'Laki-laki', '20518848', '23242-20518848-6', '2024-01-06 10:15:05', '2024-01-06 10:15:05' ),
( '281', '0115206653', 'MOHAMAD RIDHO', 'Laki-laki', '20518848', '23242-20518848-6', '2024-01-06 10:15:05', '2024-01-06 10:15:05' ),
( '282', '0107922700', 'MUHAMAD MAULANA', 'Laki-laki', '20518848', '23242-20518848-6', '2024-01-06 10:15:05', '2024-01-06 10:15:05' ),
( '283', '0116890221', 'MUHAMAD RIZAL SETIYAWAN', 'Laki-laki', '20518848', '23242-20518848-6', '2024-01-06 10:15:05', '2024-01-06 10:15:05' ),
( '284', '0117592095', 'Muhammad Dimas Okta Firdaus', 'Laki-laki', '20518848', '23242-20518848-6', '2024-01-06 10:15:05', '2024-01-06 10:15:05' ),
( '285', '0119480189', 'MUHAMMAD NIZAM UBAYDILLAH ASYIFA', 'Laki-laki', '20518848', '23242-20518848-6', '2024-01-06 10:15:05', '2024-01-06 10:15:05' ),
( '286', '0123445336', 'Muhammad Yahya Habibi', 'Laki-laki', '20518848', '23242-20518848-6', '2024-01-06 10:15:05', '2024-01-06 10:15:05' ),
( '287', '0116613144', 'RENO ADE SYAHPUTRA', 'Laki-laki', '20518848', '23242-20518848-6', '2024-01-06 10:15:05', '2024-01-06 10:15:05' ),
( '288', '0108552295', 'RERE DEWI ANDINI', 'Perempuan', '20518848', '23242-20518848-6', '2024-01-06 10:15:05', '2024-01-06 10:15:05' ),
( '289', '0114537036', 'RISMA AYU RAHMAWATI', 'Perempuan', '20518848', '23242-20518848-6', '2024-01-06 10:15:05', '2024-01-06 10:15:05' ),
( '290', '0115160229', 'RIYAN ADI PRATAMA', 'Laki-laki', '20518848', '23242-20518848-6', '2024-01-06 10:15:05', '2024-01-06 10:15:05' ),
( '291', '0109242325', 'SAIFUL KIAN ADITIYA PRATAMA', 'Laki-laki', '20518848', '23242-20518848-6', '2024-01-06 10:15:05', '2024-01-06 10:15:05' ),
( '292', '0115723455', 'SHAFA MAULA ARDI ANGELINA', 'Perempuan', '20518848', '23242-20518848-6', '2024-01-06 10:15:05', '2024-01-06 10:15:05' ),
( '293', '0111240914', 'SHIVIANAYLA AZZAHRA HARIANTO', 'Perempuan', '20518848', '23242-20518848-6', '2024-01-06 10:15:05', '2024-01-06 10:15:05' ),
( '294', '0115613001', 'TASYA NAFSI AFSYANI', 'Perempuan', '20518848', '23242-20518848-6', '2024-01-06 10:15:05', '2024-01-06 10:15:05' ),
( '295', '0135092021', 'Temmy Maulana', 'Laki-laki', '20518848', '23242-20518848-6', '2024-01-06 10:15:05', '2024-01-06 10:15:05' ),
( '296', '0112715746', 'Wildan Basitu Aladzim', 'Laki-laki', '20518848', '23242-20518848-6', '2024-01-06 10:15:05', '2024-01-06 10:15:05' ),
( '297', '0108924474', 'WULAN LESTARI', 'Perempuan', '20518848', '23242-20518848-6', '2024-01-06 10:15:06', '2024-01-06 10:15:06' ),
( '298', '0123543405', 'Yusuf Al Khodri', 'Laki-laki', '20518848', '23242-20518848-6', '2024-01-06 10:15:06', '2024-01-06 10:15:06' );
COMMIT;
-- ---------------------------------------------------------


-- Dump data of "users" ------------------------------------
BEGIN;

INSERT INTO `users`(`id`,`name`,`email`,`email_verified_at`,`password`,`level`,`remember_token`,`guru_id`,`created_at`,`updated_at`) VALUES 
( '1', 'bejo', 'bejo@admin.com', NULL, '$2y$10$hV.FRA4nslev5bR3k8w.9uhSrf5h1PXmYLmtS4TBHMPTCjHaU0k7i', 'admin', NULL, '1', '2024-01-06 08:50:52', '2024-01-06 08:50:52' ),
( '6', 'matsoleh', 'muhammadsoleh37@guru.sd.belajar.id', NULL, '$2y$10$uylW5pho1S23z.r/9Y.GYevL9NJuvyjdHX3PMjB2laNbQShIKTXKa', 'gpai', NULL, '2', '2024-01-06 09:31:15', '2024-01-06 09:31:15' ),
( '7', 'fitri', 'fitri@kkgpaiwagir.or.id', NULL, '$2y$10$f9/9xdA5XC/m8gWRGWEN1.1mrE8WYq3lgkinKZAocSU33N4qKcMMi', 'gpai', NULL, '137', '2024-01-09 13:30:55', '2024-01-09 13:30:55' );
COMMIT;
-- ---------------------------------------------------------


/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
-- ---------------------------------------------------------


