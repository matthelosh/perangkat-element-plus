import './bootstrap';
import '../css/app.css'

import { createApp, h } from 'vue';
import { createInertiaApp } from '@inertiajs/vue3';
import VCalendar from 'v-calendar';
import 'v-calendar/style.css';
import VDatePicker from 'v-calendar';
import VueExcelEditor from 'vue3-excel-editor';
import VueApexCharts from "vue3-apexcharts";
// import ElementTiptapPlugin from 'element-tiptap-vue3-fixed';
// import ElementTiptap's styles
// import 'element-tiptap/lib/style.css';


createInertiaApp({
    resolve: name => {
        const pages = import.meta.glob('./Pages/**/*.vue', {eager: true})
        return pages[`./Pages/${name}.vue`] ? pages[`./Pages/${name}.vue`] : pages[`./Pages/Default.vue`] 
    },

    setup({ el, App, props, plugin}) {
        createApp({ render: () => h(App, props)})
            .use(plugin)
            .use(VCalendar, VDatePicker)
            .use(VueExcelEditor)
            .use(VueApexCharts)
            .mixin({ methods: { zRoute: route }})
            .mount(el)
    },
})