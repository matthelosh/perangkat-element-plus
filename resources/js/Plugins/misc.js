const defaultImage = (e) => {
    return e.target.src = '/img/img-placeholder.jpg';
}

export default defaultImage