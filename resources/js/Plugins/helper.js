import dayjs from 'dayjs'
import 'dayjs/locale/id'
import localeData from 'dayjs/plugin/localeData'
import axios from 'axios'

dayjs.extend(localeData)
dayjs.locale("id")
dayjs().localeData()

import { usePage } from '@inertiajs/vue3'
const page = usePage()

const bulans = dayjs.months()
const haris = dayjs.weekdays()


export const capitalize = (s) => s && s.length > 0 && s[0].toUpperCase() + s.slice(1);
export const uppercase = (s) => s.toUpperCase();
export const weeksPerMonth = (month, hari) => {
    let monthNumber = bulans.lastIndexOf(month)
    let year = page.props.periode.semester == '1' ? page.props.periode.tapel.split("/")[0] : page.props.periode.tapel.split("/")[1]
    let date = dayjs(new Date(year+'-'+ (monthNumber+1)))
    let weekday = haris.lastIndexOf(hari)
    let dif = (7 + (weekday - date.day())) % 7 + 1
    let totalWeeks = Math.floor((date.daysInMonth() - dif) / 7) +1
   
    return totalWeeks
    
}

export const allUnefektif = (months, day) => {
    let dates = []
    for(let m of months) {
        if (unefektif(m, day).length > 0 ) {
            dates.push(unefektif(m, day).map(d => d.start))
        }
    }

    return dates
}

export const unefektif = (month, day) => {
    let events = [];
    page.props.agendas.forEach(kaldik => {
        let rentang = dayjs(kaldik.end).date() - dayjs(kaldik.start).date()
        if ( rentang < 1 ) {
            if ((bulans.lastIndexOf(month)) == dayjs(kaldik.start).get('month') && haris.lastIndexOf(day) == dayjs(kaldik.start).get('day')) {
                events.push(kaldik)
            }
            
        } else {
            let ranges = [];
            for (let h = 0; h < rentang; h++ ) {
                let hari =  dayjs(kaldik.start).add(h, 'day').get('day');
                let bulan = dayjs(kaldik.start).add(h, 'day').get('month')
                if (hari == haris.lastIndexOf(day) && bulan == bulans.lastIndexOf(month)) {
                    events.push({
                        bulan: bulan,
                        hari: hari,
                        name: kaldik.name,
                        description: kaldik.description,
                        start: dayjs(kaldik.start).add(h, 'day').format('YYYY-MM-DD'),
                        end: dayjs(kaldik.start).add(h, 'day').format('YYYY-MM-DD'),
                        location: kaldik.location,
                        color: kaldik.color,
                        is_libur: kaldik.is_libur,
                        is_active: kaldik.is_active
                    })
                }
            }
        } 
    })
    return events
}

export const sumEfektif = (months, day) => {
    return weekPerSemester(months, day) - sumUnEfektif(months, day)
}

export const sumUnEfektif = (months, day) => {
    let total = 0;
    for( let m of months) {
        total += unefektif(m, day).length
    }

    return total
}

export const weekPerSemester = (months, day) => {
    let total = 0;
    for( let m of months) {
        total += weeksPerMonth(m, day)
    }

    return total
}