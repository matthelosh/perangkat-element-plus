<?php
namespace App\Http\Controllers;

// use App\Http\Controllers\Auth\UserController;

use App\Models\Menu;
use Inertia\Inertia;
use App\Models\Element;
use App\Models\Sekolah;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return Inertia::render('Welcome');
});

Route::get("/tes", function() {
    return response()->json([Menu::all()]);
});
Route::post('/kaldik', [KaldikController::class, 'index'])->name('kaldik.index');

Route::get('/login', function() {
    return redirect('/');
});

Route::post('/login', [Auth\UserController::class, 'login'])->name('login');

Route::group(['middleware' => 'auth'], function() {

    Route::inertia('/about', 'About')->middleware('auth')->name('about');

    Route::get('/beranda', [DashboardController::class, 'home'])->middleware('auth')->name('beranda');

    Route::prefix('sekolah')->group(function() {
        Route::inertia('/', 'Dashboard/Utama/Sekolah')->name('sekolah');
        Route::post('/', [SekolahController::class, 'index'])->name('sekolah.index');
        Route::post('/store', [SekolahController::class, 'store'])->name('sekolah.store');
        Route::post('/impor', [SekolahController::class, 'impor'])->name('sekolah.impor');
        Route::delete('/{id}', [SekolahController::class, 'destroy'])->name('sekolah.destroy');
    });

    Route::prefix('guru')->group(function() {
        Route::get('/', [DashboardController::class, 'guru'])->name('guru');
        Route::post('/', [GuruController::class, 'index'])->name('guru.index');
        Route::post('/store', [GuruController::class, 'store'])->name('guru.store');
        Route::post('/create-account', [GuruController::class, 'createAccount'])->name('guru.create-account');
        Route::post('/impor', [GuruController::class, 'impor'])->name('guru.impor');
        Route::delete('/{id}', [GuruController::class, 'destroy'])->name('guru.destroy');
        Route::post('/{id}/remove-account', [GuruController::class, 'removeAccount'])->name('guru.remove-account');
    });
// Master
Route::inertia('/kaldik', 'Dashboard/Kaldik')->name('kaldik.admin');

    
// Rencana
    Route::prefix('rencana')->group(function() {
        Route::get('/', [DashboardController::class, 'rencana'])->name('rencana');

        Route::prefix('kaldik')->group(function() {
            Route::inertia('/', 'Dashboard/Kaldik')->name('kaldik');
            
            Route::post('/store', [KaldikController::class, 'store'])->name('kaldik.store');
            Route::delete('/{id}', [KaldikController::class, 'destroy'])->name('kaldik.destroy');
        });

        Route::prefix('jadwal')->group(function () {
            Route::inertia('/','Dashboard/Jadwal')->name('jadwal');
            Route::post('/', [JadwalController::class, 'index'])->name('jadwal.index');
            Route::post('/store', [JadwalController::class, 'store'])->name('jadwal.store');
            Route::delete('/{id}', [JadwalController::class, 'destroy'])->name('jadwal.destroy');
            Route::put('/{id}', [JadwalController::class, 'update'])->name('jadwal.update');
           
        });

        Route::prefix('prota')->group(function() {
            // Route::inertia('/', 'Dashboard/Prota')->name('prota.page');
            Route::post('/', [ProtaController::class, 'index'])->name('prota.index');
            Route::post('/store', [ProtaController::class, 'store'])->name('prota.store');
        });

        Route::prefix('prosem')->group(function() {
            Route::post('/', [ProsemController::class, 'index'])->name('prosem.index');
            Route::post('/store', [ProsemController::class, 'store'])->name('prosem.store');
        });

        Route::prefix('pembelajaran')->group(function() {
            Route::post('/', [PembelajaranController::class, 'index'])->name('pembelajaran.index');
        });

        Route::prefix('materi')->group(function() {
            Route::post('/protasem/store', [MateriController::class, 'storeProtasem'])->name('materi.protasem.store');
        });

        // CP
        Route::prefix('cp')->group(function() {
            Route::post("/", [CpController::class, 'index'])->name('cp.index');
        });
        // TP
        Route::prefix('tp')->group(function() {
            Route::get("/", [TpController::class, 'home'])->name('tp');
            Route::post("/store", [TpController::class, 'store'])->name('tp.store');
            Route::delete("/", [TpController::class, 'destroy'])->name('tp.destroy');
        });
        // ATP
        Route::prefix("atp")->group(function() {
            Route::post("/", [AtpController::class, 'index'])->name('atp.index');
            Route::post("/store", [AtpController::class, 'store'])->name('atp.store');
        });
    });

    

    


    Route::prefix('periode')->group(function() {
        Route::inertia('/', 'Dashboard/Periode')->name('periode');
        Route::post('/', [PeriodeController::class, 'index'])->name('periode.index');
        Route::post('/store', [PeriodeController::class, 'store'])->name('periode.store');
        Route::put('/{id}', [PeriodeController::class, 'toggle'])->name('periode.toggle');
    });

    Route::prefix('rombel')->group(function() {
        Route::get('/', [RombelController::class, 'page'])->name('rombel');
        Route::post('/', [RombelController::class, 'index'])->name('rombel.index');
        Route::post('/store', [RombelController::class, 'store'])->name('rombel.store');
        Route::post('/impor-siswa', [RombelController::class, 'imporSiswa'])->name('rombel.impor-siswa');
        Route::delete('/{id}', [RombelController::class, 'destroy'])->name('rombel.destroy');
    });
    Route::prefix('siswa')->group(function() {
        Route::get('/', [SiswaController::class, 'viewSiswa'])->name('siswa');
        Route::delete('/by-rombel/{rombel}', [SiswaController::class, 'destroyByRombel'])->name('siswa.delete.by-rombel');
    });

    

    Route::prefix('pelaksanaan')->group(function() {
        Route::get('/', [DashboardController::class, 'pelaksanaan'])->name('pelaksanaan');
    });
    
    Route::prefix('evaluasi')->group(function() {
        Route::inertia('/', 'Dashboard/Evaluasi/Index')->name('evaluasi');
    });

    Route::prefix('profil')->group(function() {
        Route::inertia('/', 'Dashboard/Profil')->name('profil');
    });

    Route::prefix('setting')->group(function() {
        Route::get('/', [SettingController::class, 'page'])->name('setting');
    });

    Route::get('/logout', function() {
        auth()->logout();
        return redirect('/');
    })->name('logout');

});